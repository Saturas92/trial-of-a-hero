#pragma once
#include "GameObject.h"
#include <glm/glm/glm.hpp>

using namespace glm;

class GameObjStrWall :
	public GameObject
{
public:
	GameObjStrWall(aiNode *mesh, const aiScene *scene, map<string, GLuint> textureIDMap, GLfloat Xmax, GLfloat Xmin, GLfloat Ymax, GLfloat Ymin, GLfloat Zmax, GLfloat Zmin);
	~GameObjStrWall();

	GLboolean collidesWith(vec3 destination);
private:
	GLfloat Xmax;
	GLfloat Xmin;
	GLfloat Ymax;
	GLfloat Ymin;
	GLfloat Zmax;
	GLfloat Zmin;


};

