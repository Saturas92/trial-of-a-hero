#include <glm/glm/glm.hpp>
#include <GL/glut.h>
#pragma once

using namespace glm;

enum Camera_Mov {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};

const GLfloat YAW = -90.0f;
const GLfloat PITCH = 0.0f;
const GLfloat SPEED = 3.0f;
const GLfloat ROTSPEED = 100.0f;
const GLfloat SENSITIVITY = 0.20f;
const GLfloat HEIGHT = 1.8f;

class Camera
{
public:
	vec3 Position;
	vec3 Front; //direzione in cui guarda la telecamera
	vec3 Up;

	vec3 Destination;
	
	GLfloat Yaw;
	GLfloat Pitch;
	GLfloat MovementSpeed;
	GLfloat MouseSensitivity;
	GLfloat RotationSpeed;

	GLfloat DestinationYaw;
	GLfloat DestinationPitch;

	void move(Camera_Mov direction, GLfloat dt);
	void move(vec3 position);
	void rotateInt(GLfloat x, GLfloat y);
	void rotateStr(GLfloat x, GLfloat y);
	vec3 testPos(Camera_Mov direction, GLfloat dt);
	vec3 testPos(vec3 direction, GLfloat dt);
	
	//Funzioni per il movimento automatico della telecamera
	void setDestination(vec3 destination);
	GLboolean testDestination();
	void moveTowardsDestination(GLfloat dt);

	//Funzioni per la rotazione automatica della telecamera
	void setDestinationYaw(GLfloat destinationYaw);
	GLboolean testDestinationYaw();
	void rotateTowardsDestinationYaw(GLfloat dt);

	void setDestinationPitch(GLfloat destinationPitch);
	GLboolean testDestinationPitch();
	void rotateTowardsDestinationPitch(GLfloat dt);


	Camera(vec3 position = vec3(0.0f,HEIGHT,0.0f), vec3 up = vec3(0.0f, 1.0f, 0.0f), GLfloat yaw = YAW, GLfloat pitch = PITCH);
	~Camera();
	void updateFrontVector();
};

