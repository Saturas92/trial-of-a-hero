#include "GameObjProjectile.h"


GameObjProjectile::GameObjProjectile(aiNode * meshEnable, const aiScene * scene, map<string, GLuint> textureIDMap, vec3 position, GLfloat yaw, GLfloat pitch, vec3 rotationCenter, GLfloat speed, GLfloat rotationSpeed, GLfloat gravity)
	:GameObject(meshEnable, scene, textureIDMap)
{
	Position = position;
	RotationCenter = rotationCenter;
	MovementSpeed = speed;
	RotationSpeed = rotationSpeed;
	Gravity = gravity;
	Time = 0.0f;

	Yaw = yaw;
	Pitch = pitch;
	Angle = 0.0f;

	// Calcolo del vettore direzione
	vec3 front;
	front.x = cos(radians(Pitch)) * cos(radians(Yaw));
	front.y = sin(radians(Pitch));
	front.z = cos(radians(Pitch)) * sin(radians(Yaw));
	Direction = normalize(front);

}

GameObjProjectile::~GameObjProjectile()
{
}

void GameObjProjectile::move(GLfloat dt)
{
	GLfloat velocity = MovementSpeed * dt;
	Position += Direction * velocity;

	Time += dt;
	Position.y -= 0.5*(Gravity * pow(Time,2));

	GLfloat angularVelocity = RotationSpeed * dt;
	Angle += angularVelocity;
}

void GameObjProjectile::draw()
{
	glPushMatrix();

	vec3 temp = normalize(cross(Direction, vec3(0.0f,1.0f,0.0f)));

	glTranslatef(Position.x, Position.y, Position.z);
	glRotatef(-Angle, temp.x, temp.y, temp.z);
	glRotatef(-Yaw, 0.0f, 1.0f, 0.0f);
	glRotatef(Pitch, 0.0f, 0.0f, 1.0f);


	GameObject::draw();
	glPopMatrix();
}