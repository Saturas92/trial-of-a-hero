#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <chrono>
#include "Game.h"
#include <GL/glut.h>
#pragma once

//http://learnopengl.com/#!In-Practice/2D-Game/Setting-up

//using namespace glm;
using namespace std;
using namespace chrono;


const GLuint SCREEN_WIDTH = 1920; // Larghezza dello schermo
const GLuint SCREEN_HEIGHT = 1080; // Altezza dello schermo

// Nome del gioco: Trial of A Hero?
Game Trial(SCREEN_WIDTH, SCREEN_HEIGHT);

// Tempo dell'ultimo frame
time_point<system_clock> lastFrame;

// Prototipi funzioni
void resize(int width, int height);
void display(void);
void pressKeyCallback(unsigned char key, int x, int y);
void releaseKeyCallback(unsigned char key, int x, int y);
void mouseButtonCallback(int button, int state, int x, int y);
void mouseMoveCallback(int x, int y);

int main(int argc, char *argv[]) {

	// Configurazioni di OpenGL
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	/*glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	glutCreateWindow("Trial of A Hero");*/

	glutGameModeString("1920x1080:32@60"); //the settings for fullscreen mode
	glutEnterGameMode();

	glutDisplayFunc(display);
//	glutReshapeFunc(resize);
	glutKeyboardFunc(pressKeyCallback);
	glutKeyboardUpFunc(releaseKeyCallback);
	glutMouseFunc(mouseButtonCallback);
	glutPassiveMotionFunc(mouseMoveCallback);
	glutIdleFunc(display);

	// Inizializza il gioco
	Trial.Init();

	lastFrame = system_clock::now();

	glutMainLoop();

	return 0;
}

void resize(int width, int height) {
	// Non si vuole permettere un ridimensionamento della finestra
	glutReshapeWindow(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(90.0f, glutGet(GLUT_WINDOW_WIDTH) * 1.0 / glutGet(GLUT_WINDOW_HEIGHT), 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

}


void display(void)
{
	// Calcolo del delta time
	time_point<system_clock> now = system_clock::now();
	duration<GLfloat> temp = now - lastFrame;
	GLfloat deltaTime = temp.count();
	lastFrame = now;
	//cout << ((float)deltaTime * 1000.0) << endl;

	// Gestisce input utente e aggiorna gli elementi del gioco
	Trial.Update(deltaTime);

	// Esegue il render
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Trial.Render();

	glutSwapBuffers();

}

void pressKeyCallback(unsigned char key, int x, int y) {
	/*if (key == 27) {
		exit(1);
	}*/
	if (key < 1024) {
		Trial.Keys[key] = GL_TRUE;
	}
}

void releaseKeyCallback(unsigned char key, int x, int y) {
	if (key < 1024) {
		Trial.Keys[key] = GL_FALSE;
	}
}

void mouseButtonCallback(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN) {
			Trial.LMouse = GL_TRUE;
			Trial.LMouseX = x;
			Trial.LMouseY = y;
		}

		else if (state == GLUT_UP)
			Trial.LMouse = GL_FALSE;
	}

}

void mouseMoveCallback(int x, int y) {
	Trial.mouseMove(x, y);
}
