#include <iostream>
#include <chrono>
#include <thread>
#include <random>
#include "irrKlang.h"
#include "Game.h"
#include "SceneLoader.h"
#include "SoundHandler.h"

using namespace std;
using namespace irrklang;

Camera *Player;
SceneLoader *IntLoader;
SceneLoader *StrLoader;
ISoundEngine *SoundEngine;
ISound *MenuMusic;
ISound *IntMusic;
ISound *StrMusic;
SoundHandler *Footsteps;
SoundHandler *DoorSliding;
SoundHandler *DoorOpening;
SoundHandler *StatueLighting;
SoundHandler *SmallDoorOpening;
SoundHandler *SmallDoorClosing;
SoundHandler *LavaBubbling;
SoundHandler *AxeThrowing;
SoundHandler *StatueCrushing;

bool captureMouse = false;
bool firstMouse = true;
GLfloat lastX, lastY;
random_device generator;
uniform_int_distribution<int> distribution;

GLfloat StrGolemSpeed;


Game::Game(GLuint width, GLuint height)
	: State(GAME_MENU), Keys(), Width(width), Height(height), LMouse()
{
}

Game::~Game()
{
	delete IntRoom;
	for (auto i = IntCollision.begin(); i != IntCollision.end(); i++) {
		delete (*i);
	}
	for (auto i = IntDragonEggs.begin(); i != IntDragonEggs.end(); i++) {
		delete (*i);
	}
	delete IntEntranceDoorL;
	delete IntEntranceDoorR;
	delete IntExitDoorL;
	delete IntExitDoorR;
	delete IntFire;

	for (auto i = StrGolems.begin(); i != StrGolems.end(); i++) {
		delete (*i);
	}
	delete StrWall;
	delete StrWallRoom;
	delete StrGolemModel;
	delete StrAxeModel;
	delete StrRoom;
	delete StrLava;
	delete StrEntranceDoor;
	delete StrExitDoorL;
	delete StrExitDoorR;

	delete Player;
	delete IntLoader;
	delete StrLoader;
	delete SoundEngine;

	delete MenuMusic;
	delete IntMusic;
	delete StrMusic;
	delete Footsteps;
	delete DoorSliding;
	delete DoorOpening;
	delete StatueLighting;
	delete SmallDoorOpening;
	delete SmallDoorClosing;
	delete LavaBubbling;
	delete AxeThrowing;
	delete StatueCrushing;

}

void Game::Init()
{
	ilInit(); /* Initialization of DevIL */

	Width = glutGet(GLUT_WINDOW_WIDTH);
	Height = glutGet(GLUT_WINDOW_HEIGHT);

	Player = new Camera();

	// Variabili Intelligenza
	IntLoader = new SceneLoader("./models/game_int.dae");
	IntScene = IntLoader->Scene;
	IntTextureIDMap = IntLoader->TextureIdMap;
	IntRoom = new GameObjIntWall(IntScene->mRootNode->FindNode("Int_Stanza"), IntScene, IntTextureIDMap);

	for (int i = 1; i <= 8; i++) {
		GameObjBB *IntStatue = new GameObjBB(IntScene->mRootNode->FindNode("Int_Drago" + to_string(i)), IntScene, IntTextureIDMap);
		GameObject *IntDragonEgg = new GameObject(IntScene->mRootNode->FindNode("Int_Drago" + to_string(i) + "_On"), IntScene, IntTextureIDMap, IntScene->mRootNode->FindNode("Int_Drago" + to_string(i) + "_Off"), STATE_DISABLE);
		IntCollision.push_back(IntStatue);
		IntDragonEggs.push_back(IntDragonEgg);
	}

	IntDragonAnimated = new GameObjBB_SkeletalAnimation(IntScene->mRootNode->FindNode("Int_Drago1"), IntScene, IntTextureIDMap, IntLoader, 0, true, vec3(-2.11189,-1.24603,5.07359));

	/*for (int i = 0; i < IntLoader->Animations.size(); i++) {
		cout << "Anim " + to_string(i) + " (duration, tickspersecond): " << IntLoader->Animations[i].Duration << " " << IntLoader->Animations[i].TicksPerSecond << endl;
	}*/

	IntEntranceDoorL = new GameObjBB(IntScene->mRootNode->FindNode("Int_Porta_Ingresso_sx"), IntScene, IntTextureIDMap);
	IntEntranceDoorR = new GameObjBB(IntScene->mRootNode->FindNode("Int_Porta_Ingresso_dx"), IntScene, IntTextureIDMap);

	IntExitDoorL = new GameObjBB(IntScene->mRootNode->FindNode("Int_Porta_Uscita_sx"), IntScene, IntTextureIDMap, vec3(-0.48802, 0, -0.12126));
	IntExitDoorR = new GameObjBB(IntScene->mRootNode->FindNode("Int_Porta_Uscita_dx"), IntScene, IntTextureIDMap, vec3( 0.48802, 0, -0.12126));

	IntFire = new GameObjBB(IntScene->mRootNode->FindNode("Int_Fuoco"), IntScene, IntTextureIDMap);

	// Variabili Forza
	StrLoader = new SceneLoader("./models/game_str.dae");
	StrScene = StrLoader->Scene;
	StrTextureIDMap = StrLoader->TextureIdMap;
	StrWall = new GameObjStrWall(NULL, StrScene, StrTextureIDMap, 3.1f, -3.1f, 3, 0, 17.0f, 16.0f);
	StrWallRoom = new GameObjStrWall(NULL, StrScene, StrTextureIDMap, 8.3f, -8.3f, 9.0f, -1.1f, 19.0f, -5.0f);
	StrAxeModel = new GameObject(StrScene->mRootNode->FindNode("Str_Ascia"), StrScene, StrTextureIDMap);
	StrGolemModel = new GameObject(StrScene->mRootNode->FindNode("Str_Golem"), StrScene, StrTextureIDMap, StrScene->mRootNode->FindNode("Str_Golem_Distrutto"));
	StrLava = new GameObject(StrScene->mRootNode->FindNode("Str_Lava"), StrScene, StrTextureIDMap);
	StrRoom = new GameObject(StrScene->mRootNode->FindNode("Str_Stanza"), StrScene, StrTextureIDMap);
	StrEntranceDoor = new GameObjBB(StrScene->mRootNode->FindNode("Str_Porta_Ingresso"), StrScene, StrTextureIDMap, vec3(-0.892766, 0, 0), NULL, STATE_ENABLE, GL_TRUE, 1.0f, 60.0f);
	StrExitDoorL = new GameObjBB(StrScene->mRootNode->FindNode("Str_Porta_Uscita_sx"), StrScene, StrTextureIDMap, vec3(-0.6233164, 0, -0.1813));
	StrExitDoorR = new GameObjBB(StrScene->mRootNode->FindNode("Str_Porta_Uscita_dx"), StrScene, StrTextureIDMap, vec3( 0.6233164, 0, -0.1813));

	StrGolemNum = 20;

	// Inizializzazione del vettore StrSpawnPosition
	StrSpawnPosition.push_back(vec3(-3.75f, 0, 1.0f));
	StrSpawnPosition.push_back(vec3(-2.25f, 0, 1.0f));
	StrSpawnPosition.push_back(vec3(-0.75f, 0, 1.0f));
	StrSpawnPosition.push_back(vec3( 0.75f, 0, 1.0f));
	StrSpawnPosition.push_back(vec3( 2.25f, 0, 1.0f));
	StrSpawnPosition.push_back(vec3( 3.75f, 0, 1.0f));


	//Impostazioni globali

	glEnable(GL_TEXTURE_2D);
	glShadeModel(GL_SMOOTH);		 // Enables Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0f);				// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);		// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);			// The Type Of Depth Test To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculation

	glViewport(0, 0, glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));

	glEnable(GL_LIGHTING);
	glEnable(GL_NORMALIZE);
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
	GLfloat LightAmbient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat LightDiffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat IntLightPosition[] = { 0.0f, 5.0f, 0.0f, 1.0f };
	//Luce per l'intelligenza
	glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT0, GL_POSITION, IntLightPosition);

	//Luci per la forza
	GLfloat StrLightPosition1[] = { 0.0f, 5.0f, 16.0f, 1.0f };

	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);
	glLightfv(GL_LIGHT1, GL_POSITION, StrLightPosition1);

	glColorMaterial(GL_FRONT_AND_BACK, GL_DIFFUSE);

	// Suoni
	SoundEngine = createIrrKlangDevice();
	MenuMusic = SoundEngine->play2D("audio/Soliloquy.mp3", GL_TRUE, GL_TRUE, GL_TRUE);
	MenuMusic->setVolume(0.2);
	IntMusic = SoundEngine->play2D("audio/magical_theme.ogg", GL_TRUE, GL_TRUE, GL_TRUE);
	IntMusic->setVolume(0.2);
	StrMusic = SoundEngine->play2D("audio/Heroic_Demise.mp3", GL_TRUE, GL_TRUE, GL_TRUE);
	StrMusic->setVolume(0.2);

	MenuInit();

	Footsteps = new SoundHandler(SoundEngine, "audio/footstep.wav", 0.5f, 1);
	DoorSliding = new SoundHandler(SoundEngine, "audio/door_sliding.wav", 0.1f, 1);
	DoorOpening = new SoundHandler(SoundEngine, "audio/door_opening.wav", 0.1f, 1);
	StatueLighting = new SoundHandler(SoundEngine, "audio/statue_lighting.wav", 0.1f, 0.6);
	SmallDoorOpening = new SoundHandler(SoundEngine, "audio/small_door_opening.ogg", 0.1f, 0.6);
	SmallDoorClosing = new SoundHandler(SoundEngine, "audio/small_door_closing.ogg", 0.1f, 0.6);
	LavaBubbling = new SoundHandler(SoundEngine, "audio/lava_bubbling.ogg", 0.1f, 0.6);
	AxeThrowing = new SoundHandler(SoundEngine, "audio/axe_throwing.wav", 0.1f, 0.6);
	StatueCrushing = new SoundHandler(SoundEngine, "audio/statue_crushing.ogg", 0.1f, 0.6);

	glutWarpPointer(Width / 2, Height / 2);
}

void Game::MenuInit()
{
	glutSetCursor(GLUT_CURSOR_LEFT_ARROW);

	IntMusic->setIsPaused(true);
	StrMusic->setIsPaused(true);

	MenuMusic->setPlayPosition(0.0f);
	MenuMusic->setIsPaused(false);
}


void Game::IntInit()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(80.0f, glutGet(GLUT_WINDOW_WIDTH) * 1.0 / glutGet(GLUT_WINDOW_HEIGHT), 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glutSetCursor(GLUT_CURSOR_NONE);

	MenuMusic->setIsPaused(true);
	StrMusic->setIsPaused(true);

	IntMusic->setPlayPosition(0.0f);
	IntMusic->setIsPaused(false);

	Player->Position.x = 0.0f;
	Player->Position.y = HEIGHT;
	Player->Position.z = 13.0f;
	Player->Yaw = -90.0f;
	Player->Pitch = 0.0f;
	Player->updateFrontVector();


	Player->setDestination(vec3(0.0f, HEIGHT, 3.5f));

	IntExitDoorL->Yaw = IntExitDoorL->OriginalYaw;
	IntExitDoorR->Yaw = IntExitDoorR->OriginalYaw;

	for (auto i = IntDragonEggs.begin(); i != IntDragonEggs.end(); i++) {
		(*i)->State = STATE_DISABLE;
	}

	IntOpenEntranceDoors = GL_FALSE;
	IntInteractionTimeout = GL_FALSE;
	IntInteractionTimeoutAcc = 0;
	IntEnableCounter = 0;

	glEnable(GL_LIGHT0);

	glutWarpPointer(Width / 2, Height / 2);
}

void Game::StrInit()
{
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(70.0f, glutGet(GLUT_WINDOW_WIDTH) * 1.0 / glutGet(GLUT_WINDOW_HEIGHT), 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glutSetCursor(GLUT_CURSOR_NONE);

	MenuMusic->setIsPaused(true);
	IntMusic->setIsPaused(true);

	StrMusic->setPlayPosition(0.0f);
	StrMusic->setIsPaused(false);

	Player->Position.x = 0.0f;
	Player->Position.y = HEIGHT;
	Player->Position.z = 24.5f;
	Player->Yaw = -90.0f;
	Player->Pitch = 0.0f;
	Player->updateFrontVector();

	Player->setDestination(vec3(0.0f, HEIGHT, 16.5f));

	distribution = uniform_int_distribution<int>(0, StrSpawnPosition.size() - 1);

	StrGolems.clear();
	StrAxes.clear();
	StrExitDoorL->Yaw = StrExitDoorL->OriginalYaw;
	StrExitDoorR->Yaw = StrExitDoorR->OriginalYaw;

	for (int i = 0; i < StrGolemNum; i++) {
		GameObjBB *golem = new GameObjBB(StrGolemModel->MeshEnable, StrGolemModel->Scene, StrTextureIDMap, vec3(0, 0, 0), StrGolemModel->MeshDisable, STATE_ENABLE, GL_FALSE, StrGolemSpeed);
		int j = distribution(generator);
		golem->Position.x += StrSpawnPosition[j].x;
		golem->Position.y += StrSpawnPosition[j].y;
		golem->Position.z += StrSpawnPosition[j].z;
		StrGolems.push_back(golem);
	}

	StrGolemSpeed = 0.2;
	StrInteractionTimeout = GL_FALSE;
	StrInteractionTimeoutAcc = 0;
	StrSpawnTimeoutAcc = 0;
	StrGolemActivationIndex = 0;
	StrLives = 3;
	StrScore = 0;

	StrOpenEntranceDoor = GL_FALSE;

	glEnable(GL_LIGHT1);

	glutWarpPointer(Width / 2, Height / 2);
}

void Game::Update(GLfloat dt)
{
	//Aggiornamento sound handlers
	Footsteps->update(dt);
	DoorSliding->update(dt);
	DoorOpening->update(dt);
	StatueLighting->update(dt);
	SmallDoorOpening->update(dt);
	SmallDoorClosing->update(dt);
	LavaBubbling->update(dt);
	AxeThrowing->update(dt);
	StatueCrushing->update(dt);

	if (State == GAME_MENU) {
		if (LMouse == GL_TRUE) {
			if (LMouseX >= (0.1958*Width) && LMouseX <= (0.8*Width)) {
				if (LMouseY >= (0.4352*Height) && LMouseY <= (0.5407*Height)) {
					IntInit();
					State = GAME_INTELLIGENCE_INTRO_1;
				}
				else if (LMouseY >= (0.5778*Height) && LMouseY <= (0.6833*Height)) {
					StrInit();
					State = GAME_STRENGTH_INTRO_1;
				}
				else if (LMouseY >= (0.7222*Height) && LMouseY <= (0.8278*Height)) {
					exit(1);
				}
			}
		}
		else if (Keys[27] == GL_TRUE) {
			exit(1);
		}
	}
	else if (State == GAME_INTELLIGENCE_INTRO_1) { // La telecamera si muove al centro della stanza, mentre le porte di ingresso si aprono
		if (Player->testDestination() == GL_TRUE) {
			State = GAME_INTELLIGENCE_INTRO_2;
			vec3 DoorLPos = IntEntranceDoorL->OriginalPosition;
			vec3 DoorRPos = IntEntranceDoorR->OriginalPosition;

			IntEntranceDoorL->setDestination(vec3(DoorLPos.x, DoorLPos.y, DoorLPos.z));
			IntEntranceDoorR->setDestination(vec3(DoorRPos.x, DoorRPos.y, DoorRPos.z));
			DoorSliding->play();
		}
		else {
			Player->moveTowardsDestination(dt);
			Footsteps->play();
			if (IntOpenEntranceDoors == GL_FALSE && Player->Position.z <= 11) {
				IntOpenEntranceDoors = GL_TRUE;
				vec3 DoorLPos = IntEntranceDoorL->Position;
				vec3 DoorRPos = IntEntranceDoorR->Position;
				IntEntranceDoorL->setDestination(vec3(DoorLPos.x - 0.8, DoorLPos.y, DoorLPos.z));
				IntEntranceDoorR->setDestination(vec3(DoorRPos.x + 0.8, DoorRPos.y, DoorRPos.z));
				DoorSliding->play();
			}
			if (IntOpenEntranceDoors == GL_TRUE && (IntEntranceDoorL->testDestination() == GL_FALSE || IntEntranceDoorR->testDestination() == GL_FALSE)) {
				IntEntranceDoorL->moveTowardsDestination(dt);
				IntEntranceDoorR->moveTowardsDestination(dt);
			}
		}
	}
	else if (State == GAME_INTELLIGENCE_INTRO_2) { // Le porte d'ingresso si chiudono
		if (IntEntranceDoorL->testDestination() == GL_TRUE && IntEntranceDoorR->testDestination() == GL_TRUE) {
			State = GAME_INTELLIGENCE_INTRO_3;
		}
		else {
			IntEntranceDoorL->moveTowardsDestination(dt);
			IntEntranceDoorR->moveTowardsDestination(dt);
		}
	}
	else if (State == GAME_INTELLIGENCE_INTRO_3) { // Schermata introduttiva intelligenza
		if (Keys[32] == GL_TRUE) { // Viene premuto lo spazio
			State = GAME_INTELLIGENCE;
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
			lastX = Width / 2;
			lastY = Height / 2;

		}
	}
	else if (State == GAME_INTELLIGENCE) {

		//Se si preme esc, si entra nel menu di pausa
		if (Keys[27] == GL_TRUE) {
			State = GAME_INTELLIGENCE_PAUSE;
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
		}

		vec3 futurePos = Player->Position;
		// calcolo della posizione futura della camera
		if (Keys['W'] || Keys['w'])
			futurePos += Player->testPos(FORWARD, dt);
		if (Keys['S'] || Keys['s'])
			futurePos += Player->testPos(BACKWARD, dt);
		if (Keys['A'] || Keys['a'])
			futurePos += Player->testPos(LEFT, dt);
		if (Keys['D'] || Keys['d'])
			futurePos += Player->testPos(RIGHT, dt);

		//calcolo delle collisioni su futurePos
		vec3 dir = vec3(0, 0, 0);
		if ((dir = IntRoom->collidesWith(Player->Position, futurePos)) == vec3(0, 0, 0)) //test collisione con i muri
		{
			for (auto i = IntCollision.begin(); i != IntCollision.end(); i++) //se non vi � collisione con i muri,test collisione con il resto
			{
				if ((dir = (*i)->collidesWith(Player->Position, futurePos)) != vec3(0, 0, 0)) //se vi � collisione esci dal ciclo
					break;
			}
		}
		if (dir == vec3(0, 0, 0))  //se non vi � collisione, muovi normalmente
		{
			//Se il personaggio si � mosso, riproduci il suono dei passi e aggiorna la sua posizione
			if (futurePos != Player->Position) {
				Footsteps->play();
				Player->move(futurePos);
			}
		}

		else //se vi � collisione
		{
			//in dir � salvata la direzione di spostamento
			//prodotto scalare per quantificare spostamento lungo dir 
			//chiamata a testPos per conoscere lo spostamento che avrebbe la camera 
			vec3 tempPos = Player->Position;
			tempPos += Player->testPos(dir*(dot((futurePos - Player->Position), dir) * 5), dt);
			//verifica collisione di tempPos
			if ((IntRoom->collidesWith(Player->Position, tempPos)) == vec3(0, 0, 0)) //se non c'� con i muri,controlla il resto. se invece c'� con i muri,non spostare(else vuoto)
			{
				auto i = IntCollision.begin();
				for (; i != IntCollision.end(); i++)
				{
					if (((*i)->collidesWith(Player->Position, tempPos)) != vec3(0, 0, 0)) //se collide con un elemento, esci dal ciclo
						break;

				}
				if (i == IntCollision.end()) //non ci sono state collisioni. Chiamo la funzione move e mi sposto in quel punto
				{
					Player->move(tempPos);
					Footsteps->play(); // Riproduci il suono dei passi
				}

			}

		}

		//Test sull'accumulatore di timeout
		if (IntInteractionTimeoutAcc >= 0.7f) {
			IntInteractionTimeout = GL_FALSE;
			IntInteractionTimeoutAcc = 0;
		}
		//Aggiornamento dell'accumulatore di timeout
		else if (IntInteractionTimeout == GL_TRUE) {
			IntInteractionTimeoutAcc += dt;
		}

		//Test di collisione del front con le statue
		IntRenderInteraction = GL_FALSE;
		vec3 targetPos = Player->Position + (Player->Front*GLfloat(2.0));
		for (int i = 0; i < 8; i++) {
			if (IntCollision[i]->collidesWith(Player->Position, targetPos) != vec3(0, 0, 0))
			{
				//in caso di esito positivo, setto IntRenderInteraction
				IntRenderInteraction = GL_TRUE;

				//controllo tasto interazione
				if ((Keys['E'] || Keys['e']) && IntInteractionTimeout == GL_FALSE) {
					IntInteractionTimeout = GL_TRUE;

					StatueLighting->play();

					//considero la Statua i
					if (IntDragonEggs[i]->State == STATE_ENABLE) {
						IntDragonEggs[i]->State = STATE_DISABLE;
						IntEnableCounter--;
					}
					else {
						IntDragonEggs[i]->State = STATE_ENABLE;
						IntEnableCounter++;
					}

					//considero la Statua i-1
					if (IntDragonEggs[(8 + i - 1) % 8]->State == STATE_ENABLE) {
						IntDragonEggs[(8 + i - 1) % 8]->State = STATE_DISABLE;
						IntEnableCounter--;
					}
					else {
						IntDragonEggs[(8 + i - 1) % 8]->State = STATE_ENABLE;
						IntEnableCounter++;
					}

					//considero la Statua i+1
					if (IntDragonEggs[(i + 1) % 8]->State == STATE_ENABLE) {
						IntDragonEggs[(i + 1) % 8]->State = STATE_DISABLE;
						IntEnableCounter--;
					}
					else {
						IntDragonEggs[(i + 1) % 8]->State = STATE_ENABLE;
						IntEnableCounter++;
					}

				}
				break;
			}
		}

		//Check sulla condizione di vittoria
		auto i = IntDragonEggs.begin();
		for (; i != IntDragonEggs.end(); i++) {
			if ((*i)->State == STATE_DISABLE) {
				break;
			}
		}
		if (i == IntDragonEggs.end()) {
			//in tal caso tutte le statue sono enabled e abbiamo vinto
			State = GAME_INTELLIGENCE_END_1;
			Player->setDestinationPitch(0.0f);
			Player->setDestinationYaw(-90.0f);
			Player->setDestination(vec3(0.0f, HEIGHT, 3.5f));
		}

	}
	else if (State == GAME_INTELLIGENCE_END_1) {
		if (Player->testDestinationPitch() == GL_TRUE && Player->testDestinationYaw() == GL_TRUE && Player->testDestination() == GL_TRUE) {
			State = GAME_INTELLIGENCE_END_2;
			IntExitDoorL->setDestinationYaw(-80.0f);
			IntExitDoorR->setDestinationYaw(80.0f);
			DoorOpening->play();
		}
		else {
			Player->rotateTowardsDestinationPitch(dt);
			Player->rotateTowardsDestinationYaw(dt);
			Player->moveTowardsDestination(dt);
		}
	}
	else if (State == GAME_INTELLIGENCE_END_2) {
		if (IntExitDoorL->testDestinationYaw() == GL_TRUE && IntExitDoorR->testDestinationYaw() == GL_TRUE) {
			State = GAME_INTELLIGENCE_END_3;
		}
		else {
			IntExitDoorL->rotateTowardsDestinationYaw(dt);
			IntExitDoorR->rotateTowardsDestinationYaw(dt);
		}
	}
	else if (State == GAME_INTELLIGENCE_END_3) {
		if(Keys[8] == GL_TRUE) {
			State = GAME_MENU;
			MenuInit();
			glDisable(GL_LIGHT0);
			glutWarpPointer(Width / 2, Height / 2);
		}
	}
	else if (State == GAME_INTELLIGENCE_PAUSE) {
		if (Keys[32] == GL_TRUE) { // Se si preme spazio, si riprende il gioco
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
			lastX = Width / 2;
			lastY = Height / 2;
			State = GAME_INTELLIGENCE;
		}
		else if (Keys[8] == GL_TRUE) { // Se si preme backspace si torna la menu principale
			State = GAME_MENU;
			MenuInit();
			glDisable(GL_LIGHT0);
			glutWarpPointer(Width / 2, Height / 2);
		}
	}
	else if (State == GAME_STRENGTH_INTRO_1) { // La telecamera si muove al centro della stanza, mentre la porta di ingresso si apre
		if (Player->testDestination() == GL_TRUE) {
			State = GAME_STRENGTH_INTRO_2;
			StrEntranceDoor->setDestinationYaw(0);
			SmallDoorClosing->play();
		}
		else {
			Player->moveTowardsDestination(dt);
			Footsteps->play();
			if (StrOpenEntranceDoor == GL_FALSE && Player->Position.z <= 24) {
				StrOpenEntranceDoor = GL_TRUE;
				StrEntranceDoor->setDestinationYaw(100);
				SmallDoorOpening->play();
			}
			if (StrOpenEntranceDoor == GL_TRUE && StrEntranceDoor->testDestinationYaw() == GL_FALSE) {
				StrEntranceDoor->rotateTowardsDestinationYaw(dt);
			}
		}
	}
	else if (State == GAME_STRENGTH_INTRO_2) { // La porta d'ingresso si chiude
		if (StrEntranceDoor->testDestinationYaw() == GL_TRUE) {
			State = GAME_STRENGTH_INTRO_3;
		}
		else {
			StrEntranceDoor->rotateTowardsDestinationYaw(dt);
		}
	}
	else if (State == GAME_STRENGTH_INTRO_3) { // Schermata introduttiva forza
		if (Keys[32] == GL_TRUE) { // Viene premuto lo spazio
			State = GAME_STRENGTH;
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
			lastX = Width / 2;
			lastY = Height / 2;

		}
	}
	else if (State == GAME_STRENGTH) {

		//Se si preme esc, si entra nel menu di pausa
		if (Keys[27] == GL_TRUE) {
			State = GAME_STRENGTH_PAUSE;
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
		}

		vec3 futurePos = Player->Position;
		// calcolo della posizione futura della camera
		if ((Keys['A'] || Keys['a']) && (Keys['D'] || Keys['d'])) { //Se si premono A e D contemporaneamente, non ci si sposta
		}
		else if (Keys['A'] || Keys['a']) {
			if (Player->Yaw > -175.0f && Player->Yaw < -5.0f) {
				futurePos += Player->testPos(vec3(-1, 0, 0), dt);
			}
			else if (Player->Yaw < -185.0f || Player->Yaw > 5.0f) {
				futurePos += Player->testPos(vec3(1, 0, 0), dt);
			}
		}
		else if (Keys['D'] || Keys['d']) {
			if (Player->Yaw > -175.0f && Player->Yaw < -5.0f) {
				futurePos += Player->testPos(vec3(1, 0, 0), dt);
			}
			else if (Player->Yaw < -185.0f || Player->Yaw > 5.0f) {
				futurePos += Player->testPos(vec3(-1, 0, 0), dt);
			}
		}
		else if (Keys['W'] || Keys['w']) {
			if (Player->Yaw > (-85.0f) && Player->Yaw < 85.0f) // scorri a destra
				futurePos += Player->testPos(vec3(1, 0, 0), dt);
			else if (Player->Yaw < (-95.0f) && Player->Yaw > -265.0f) // scorri a sinistra
				futurePos += Player->testPos(vec3(-1, 0, 0), dt);
		}
		else if (Keys['S'] || Keys['s']) {
			if (Player->Yaw > (-85.0f) && Player->Yaw < 85.0f) // scorri a sinistra
				futurePos += Player->testPos(vec3(-1, 0, 0), dt);
			else if (Player->Yaw < (-95.0f) && Player->Yaw > -265.0f) // scorri a destra
				futurePos += Player->testPos(vec3(1, 0, 0), dt);
		}

		//calcolo delle collisioni su futurePos
		if (StrWall->collidesWith(futurePos) == GL_FALSE) { //se non vi � collisione, muovi normalmente

															//Se il personaggio si � mosso, riproduci il suono dei passi e aggiorna la sua posizione
			if (futurePos != Player->Position) {
				Footsteps->play();
				Player->move(futurePos);
			}
		}


		//Aggiornamento posizione dei proiettili
		for (auto i = StrAxes.begin(); i != StrAxes.end();) {
			(*i)->move(dt);

			//Gestione collisioni proiettili-muri esterni
			if (StrWallRoom->collidesWith((*i)->Position) == GL_TRUE) {
				//Distruggi il proiettile
				delete (*i);
				i = StrAxes.erase(i); // Quando c'� collisione l'iteratore � aggiornato dalla erase
			}
			else {
				// Se non vi � collisione incremento l'iteratore
				i++;
			}
		}


		//Gestione creazione proiettili
		//Test sull'accumulatore di timeout
		if (StrInteractionTimeoutAcc >= 0.5f) {
			StrInteractionTimeout = GL_FALSE;
			StrInteractionTimeoutAcc = 0;
		}
		//Aggiornamento dell'accumulatore di timeout
		else if (StrInteractionTimeout == GL_TRUE) {
			StrInteractionTimeoutAcc += dt;
		}
		if (LMouse == GL_TRUE && StrInteractionTimeout == GL_FALSE) {
			StrInteractionTimeout = GL_TRUE;

			GameObjProjectile * axe = new GameObjProjectile(StrAxeModel->MeshEnable, StrAxeModel->Scene, StrAxeModel->TextureIDMap, Player->Position, Player->Yaw, Player->Pitch, vec3(0, 0.05, 0));
			StrAxes.push_back(axe);
			AxeThrowing->play();
		}

		//Gestione attivazione golem
		//Aggiornamento dell'accumulatore di timeout
		StrSpawnTimeoutAcc += dt;
		//Test sull'accumulatore di timeout
		if (StrGolemActivationIndex < StrGolemNum) {
			if (StrSpawnTimeoutAcc >= 1.5f) {
				StrSpawnTimeoutAcc = 0;
				StrGolems[StrGolemActivationIndex]->Render = GL_TRUE; // Attivazione del golem
				StrGolems[StrGolemActivationIndex]->setDestination(StrGolems[StrGolemActivationIndex]->Position + vec3(0, 3, 0)); // Imposta il movimento verso l'alto
				StrGolemActivationIndex++;
			}
		}

		//Aggiornamento velocit� globale dei golem
		StrGolemSpeed += 0.3*dt;

		for (auto i = StrGolems.begin(); i != StrGolems.end(); i++) {
			if ((*i)->Render == GL_TRUE) {

				//Gestione del tempo in cui appare il golem distrutto
				if ((*i)->State == STATE_DISABLE) {
					(*i)->DisableRender(dt);
					continue;
				}

				//Aggiornamento della velocit� delle statue attive
				(*i)->MovementSpeed = StrGolemSpeed;

				if ((*i)->testDestination() == GL_TRUE) {
					if ((*i)->Position.z == (*i)->OriginalPosition.z + 1 && (*i)->Position.y == (*i)->OriginalPosition.y + 3) { // Il Golem ha finito l'ascesa
						(*i)->setDestination((*i)->Position + vec3(0, 0, 12)); //Imposta il movimento verso la porta di ingresso
					}
					else if ((*i)->Position.z == (*i)->OriginalPosition.z + 13 && (*i)->Position.y == (*i)->OriginalPosition.y + 3) { // Gestione dell'arrivo del golem a fine corsa orizzontale
						(*i)->setDestination((*i)->Position + vec3(0, -3, 0)); // Imposta il movimento verso il basso
						LavaBubbling->play();
					}
					else if((*i)->Position.z == (*i)->OriginalPosition.z + 13 && (*i)->Position.y == (*i)->OriginalPosition.y) { // Il golem ha finito la discesa
						(*i)->Render = GL_FALSE;
						StrLives--;
					}
				}


				//Gestione movimento golem
				(*i)->moveTowardsDestination(dt);

				//Gestione collisione golem proiettili
				for (auto j = StrAxes.begin(); j != StrAxes.end(); j++) {
					if ((*i)->collidesWith((*j)->Position, (*j)->Position) != vec3(0, 0, 0)) {
						(*i)->State = STATE_DISABLE; //Mostra il golem distrutto
						delete (*j); //Distruggi il proiettile
						j = StrAxes.erase(j);
						StrScore++;
						StatueCrushing->play();
						break;
					}
				}
			}
		}

		//Check sulla condizione di sconfitta (le vite sono a 0)
		if (StrLives <= 0) {
			StrLives = 0;
			State = GAME_STRENGTH_DEFEAT;
		}

		//Check sulla condizione di vittoria (tutte le statue sono state attivate e poi disattivate)
		if (StrGolemActivationIndex == StrGolemNum) {
			auto i = StrGolems.begin();
			for (; i != StrGolems.end(); i++) {
				if ((*i)->Render == GL_TRUE) {
					break;
				}
			}
			if (i == StrGolems.end()) {
				State = GAME_STRENGTH_END_1;
				Player->setDestination(vec3(0.0f, 2.0f, 1.0f));
				Player->setDestinationYaw(-90.0f);
				Player->setDestinationPitch(0.0f);
			}
		}

	}
	else if (State == GAME_STRENGTH_END_1) {
		if (Player->testDestinationPitch() == GL_TRUE && Player->testDestinationYaw() == GL_TRUE && Player->testDestination() == GL_TRUE) {
			State = GAME_STRENGTH_END_2;
			StrExitDoorL->setDestinationYaw(-80.0f);
			StrExitDoorR->setDestinationYaw(80.0f);
			DoorOpening->play();
		}
		else {
			Player->rotateTowardsDestinationPitch(dt);
			Player->rotateTowardsDestinationYaw(dt);
			Player->moveTowardsDestination(dt);
		}
	}
	else if (State == GAME_STRENGTH_END_2) {
		if (StrExitDoorL->testDestinationYaw() == GL_TRUE && StrExitDoorR->testDestinationYaw() == GL_TRUE) {
			State = GAME_STRENGTH_END_3;
		}
		else {
			StrExitDoorL->rotateTowardsDestinationYaw(dt);
			StrExitDoorR->rotateTowardsDestinationYaw(dt);
		}
	}
	else if (State == GAME_STRENGTH_END_3) {
		if (Keys[8] == GL_TRUE) {
			State = GAME_MENU;
			MenuInit();
			glDisable(GL_LIGHT0);
			glutWarpPointer(Width / 2, Height / 2);
		}
	}
	else if (State == GAME_STRENGTH_DEFEAT) {
		// premi spazio per riprovare
		if (Keys[32] == GL_TRUE) {
			State = GAME_STRENGTH_INTRO_1;
			glDisable(GL_LIGHT1);
			StrInit();
		}
		//premi backspace per tornare al menu
		else if (Keys[8] == GL_TRUE) {
			State = GAME_MENU;
			MenuInit();
			glDisable(GL_LIGHT1);
			glutWarpPointer(Width / 2, Height / 2);
		}

	}
	else if (State == GAME_STRENGTH_PAUSE) {
		if (Keys[32] == GL_TRUE) { // Se si preme spazio, si riprende il gioco
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
			lastX = Width / 2;
			lastY = Height / 2;
			State = GAME_STRENGTH;
		}
		else if (Keys[8] == GL_TRUE) { // Se si preme backspace si torna la menu principale
			State = GAME_MENU;
			MenuInit();
			glDisable(GL_LIGHT0);
			glutWarpPointer(Width / 2, Height / 2);
		}
	}

	if (State == GAME_INTELLIGENCE_INTRO_1 || State == GAME_INTELLIGENCE_INTRO_2 || State == GAME_INTELLIGENCE_INTRO_3 || State == GAME_INTELLIGENCE || State == GAME_INTELLIGENCE_END_1 || State == GAME_INTELLIGENCE_END_2 || State == GAME_INTELLIGENCE_END_3) {
		IntDragonAnimated->updateFrame(dt);
	}
}



void Game::Render()
{
	if (State == GAME_MENU) {
		RenderImage(IntTextureIDMap.find("Menu.png")->second);
	}
	else if (State == GAME_INTELLIGENCE || State == GAME_INTELLIGENCE_INTRO_1 || State == GAME_INTELLIGENCE_INTRO_2 || State == GAME_INTELLIGENCE_INTRO_3 || State == GAME_INTELLIGENCE_END_1 || State == GAME_INTELLIGENCE_END_2 || State == GAME_INTELLIGENCE_END_3 || State == GAME_INTELLIGENCE_PAUSE) {

		if (firstMouse) {
			firstMouse = false;
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
			lastX = Width / 2;
			lastY = Height / 2;
			Player->Front = vec3(0, 0, -1);
		}

		glPushMatrix();
		glLoadIdentity();

		// Gestione telecamera
		vec3 tempPos = Player->Position;
		vec3 tempFront = tempPos + Player->Front;
		vec3 tempUp = Player->Up;
		gluLookAt(tempPos.x, tempPos.y, tempPos.z,
			tempFront.x, tempFront.y, tempFront.z,
			tempUp.x, tempUp.y, tempUp.z);

		GLfloat LightPosition[] = { 0.0f, 5.0f, 0.0f, 1.0f };
		glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);

		// Render della stanza
		IntRoom->draw();

		// Render delle statue animate
		float angle = 0;
		for (int i = 0; i < 8; i++) {
			IntDragonAnimated->Yaw = angle;
			IntDragonAnimated->drawFrame();
			angle += 45.0f;
		}

		// Render delle uova
		for (auto i = IntDragonEggs.begin(); i != IntDragonEggs.end(); i++)
		{
			if ((*i)->State == STATE_ENABLE) {
				float c[] = { 1.0f, 1.0f, 1.0f, 1.0f };
				glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);
			}
			glEnable(GL_BLEND); 
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			(*i)->draw();
			glDisable(GL_BLEND);
			float c[] = { 0.0f, 0.0f, 0.0f, 1.0f };
			glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);

		}

		// Render delle porte
		IntEntranceDoorL->draw();
		IntEntranceDoorR->draw();
		IntExitDoorL->draw();
		IntExitDoorR->draw();

		// Render dei fuochi con materiali emissivi
		float c[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		IntFire->draw();
		glDisable(GL_BLEND);
		c[0] = 0.0;
		c[1] = 0.0;
		c[2] = 0.0;
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);

		if (State == GAME_INTELLIGENCE) {
			RenderString(-0.95, 0.9, "Statue accese: " + to_string(IntEnableCounter), 1, 1, 0);
			RenderImage(IntTextureIDMap.find("Mirino.png")->second);

			if (IntRenderInteraction == GL_TRUE) {
				RenderImage(IntTextureIDMap.find("Interazione_Statua.png")->second);
			}

		}
		else if (State == GAME_INTELLIGENCE_INTRO_2) {
			RenderImage(IntTextureIDMap.find("Int_Tutorial_1.png")->second);
		}
		else if (State == GAME_INTELLIGENCE_INTRO_3) {
			RenderImage(IntTextureIDMap.find("Int_Tutorial_2.png")->second);
		}
		else if (State == GAME_INTELLIGENCE_END_2) {
			RenderImage(IntTextureIDMap.find("Win_1.png")->second);
		}
		else if (State == GAME_INTELLIGENCE_END_3) {
			RenderImage(IntTextureIDMap.find("Win_2.png")->second);
		}
		else if (State == GAME_INTELLIGENCE_PAUSE) {
			RenderImage(IntTextureIDMap.find("Pausa.png")->second);
		}

		glPopMatrix();

	}
	else if (State == GAME_STRENGTH || State == GAME_STRENGTH_INTRO_1 || State == GAME_STRENGTH_INTRO_2 || State == GAME_STRENGTH_INTRO_3 || State == GAME_STRENGTH_END_1 || State == GAME_STRENGTH_END_2 || State == GAME_STRENGTH_END_3 || State == GAME_STRENGTH_DEFEAT || State == GAME_STRENGTH_PAUSE) {
		if (firstMouse) {
			firstMouse = false;
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
			lastX = Width / 2;
			lastY = Height / 2;
			Player->Front = vec3(0, 0, -1);
		}

		glPushMatrix();
		glLoadIdentity();

		// Gestione telecamera
		vec3 tempPos = Player->Position;
		vec3 tempFront = tempPos + Player->Front;
		vec3 tempUp = Player->Up;
		gluLookAt(tempPos.x, tempPos.y, tempPos.z,
			tempFront.x, tempFront.y, tempFront.z,
			tempUp.x, tempUp.y, tempUp.z);

		GLfloat StrLightPosition1[] = { 0.0f, 5.0f, 16.0f, 1.0f };

		glLightfv(GL_LIGHT1, GL_POSITION, StrLightPosition1);

		// Render della stanza
		StrRoom->draw();
		StrEntranceDoor->draw();
		StrExitDoorL->draw();
		StrExitDoorR->draw();

		// Render dei fuochi e della lava con materiali emissivi
		float c[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		StrLava->draw();
		glDisable(GL_BLEND);
		c[0] = 0.0;
		c[1] = 0.0;
		c[2] = 0.0;
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);


		//Render dei golem
		for (auto i = StrGolems.begin(); i != StrGolems.end(); i++) {
			(*i)->draw();
		}

		// Render dei proiettili
		float c2[] = { 0.5f, 0.5f, 0.5f, 1.0f };
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c2);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		for (auto i = StrAxes.begin(); i != StrAxes.end(); i++) {
			(*i)->draw();
		}
		glDisable(GL_BLEND);
		c2[0] = 0.0;
		c2[1] = 0.0;
		c2[2] = 0.0;
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c2);

		
		if (State == GAME_STRENGTH) {
			// Render delle vite
			RenderImage(StrTextureIDMap.find("hearts" + to_string(StrLives) + ".png")->second);

			// Render del punteggio
			RenderString(-0.95, 0.9, "Punteggio: " + to_string(StrScore), 1, 1, 0);

			RenderImage(IntTextureIDMap.find("Mirino.png")->second);
		}
		else if (State == GAME_STRENGTH_INTRO_2) {
			RenderImage(IntTextureIDMap.find("Str_Tutorial_1.png")->second);
		}
		else if (State == GAME_STRENGTH_INTRO_3) {
			RenderImage(IntTextureIDMap.find("Str_Tutorial_2.png")->second);
		}
		else if (State == GAME_STRENGTH_END_2) {
			RenderImage(IntTextureIDMap.find("Win_1.png")->second);
		}
		else if (State == GAME_STRENGTH_END_3) {
			RenderImage(IntTextureIDMap.find("Win_2.png")->second);
		}
		else if (State == GAME_STRENGTH_DEFEAT) {
			// Render delle vite
			RenderImage(StrTextureIDMap.find("hearts" + to_string(StrLives) + ".png")->second);

			RenderImage(IntTextureIDMap.find("Defeat.png")->second);
		}
		else if (State == GAME_STRENGTH_PAUSE) {
			RenderImage(IntTextureIDMap.find("Pausa.png")->second);
		}



		glPopMatrix();
	}

}

void Game::RenderString(GLdouble x, GLdouble y, const std::string & string, GLfloat red, GLfloat green, GLfloat blue)
{

	glMatrixMode(GL_PROJECTION);
	glPushMatrix(); // save
	glLoadIdentity();// and clear
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST); // also disable the depth test so renders on top
	glDisable(GL_LIGHTING);

	glPushAttrib(GL_CURRENT_BIT); // salvo il colore precedente
	glColor3f(red, green, blue);

	glRasterPos2d(x, y); // center of screen(0,0). (-1,0) is center left.


	for (auto i = string.begin(); i != string.end(); i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *i);
	}

	glPopAttrib(); // ripristino il colore precedente

	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST); // Turn depth testing back on
	glEnable(GL_TEXTURE_2D);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix(); // revert back to the matrix I had before.
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();


}

void Game::RenderImage(GLuint texture)
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0.0, glutGet(GLUT_WINDOW_WIDTH), 0.0, glutGet(GLUT_WINDOW_HEIGHT), -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();


	glLoadIdentity();
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST); // also disable the depth test so renders on top


	glColor3f(1, 1, 1);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	// Draw a textured quad
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0); glVertex2f(0, glutGet(GLUT_WINDOW_HEIGHT));
	glTexCoord2f(0, 1); glVertex2f(0, 0);
	glTexCoord2f(1, 1); glVertex2f(glutGet(GLUT_WINDOW_WIDTH), 0);
	glTexCoord2f(1, 0); glVertex2f(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
	glEnd();

	glDisable(GL_BLEND);

	glDisable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	glPopMatrix();


	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);


}

void Game::mouseMove(int x, int y)
{
	if (State == GAME_INTELLIGENCE) {
		if (!captureMouse) {
			captureMouse = true;
			return;
		}
		if (x <= 150 || x >= ((Width)-150) || y <= 150 || y >= ((Height)-150)) {
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
			lastX = Width / 2;
			lastY = Height / 2;
			glutPostRedisplay();
			return;
		}


		GLfloat xoffset = x - lastX;
		GLfloat yoffset = lastY - y;

		lastX = x;
		lastY = y;

		Player->rotateInt(xoffset, yoffset);

		//cout << "Position: " << Player->Position.x << " " << Player->Position.y << " " << Player->Position.z << endl;
		//cout << "Front: " << Player->Front.x << " " << Player->Front.y << " " << Player->Front.z << endl;

	}

	else if (State == GAME_STRENGTH) {
		if (!captureMouse) {
			captureMouse = true;
			return;
		}
		if (x <= 150 || x >= ((Width)-150) || y <= 150 || y >= ((Height)-150)) {
			captureMouse = false;
			glutWarpPointer(Width / 2, Height / 2);
			lastX = Width / 2;
			lastY = Height / 2;
			glutPostRedisplay();
			return;
		}


		GLfloat xoffset = x - lastX;
		GLfloat yoffset = lastY - y;

		lastX = x;
		lastY = y;

		Player->rotateStr(xoffset, yoffset);

		//cout << "Position: " << Player->Position.x << " " << Player->Position.y << " " << Player->Position.z << endl;
		//cout << "Front: " << Player->Front.x << " " << Player->Front.y << " " << Player->Front.z << endl;
	}
}

