#pragma once
#include <glm/glm/glm.hpp>
#include "GameObject.h"

using namespace glm;

const GLfloat SPEED_PROJ = 6.0f;
const GLfloat ROTSPEED_PROJ = 1500.0f;
const GLfloat GRAVITY = 0.05;

class GameObjProjectile :
	public GameObject
{
public:
	vec3 Position; // Media dei vertici della meshEnable
	vec3 RotationCenter; // Centro di rotazione, inteso come delta rispetto a Position

	vec3 Direction; // Vettore direzione di spostamento
	GLfloat Yaw;
	GLfloat Pitch;
	GLfloat Angle; // Angolo di rotazione attorno al centro di rotazione

	GLfloat MovementSpeed;
	GLfloat RotationSpeed;
	
	GLfloat Time; // Tempo passato dalla creazione del proiettile
	GLfloat Gravity;

	GameObjProjectile(aiNode *meshEnable, const aiScene *scene, map<string, GLuint> textureIDMap, vec3 position, GLfloat yaw, GLfloat pitch, vec3 rotationCenter = vec3(0, 0, 0), GLfloat speed = (SPEED_PROJ), GLfloat rotationSpeed = ROTSPEED_PROJ, GLfloat gravity = GRAVITY);
	~GameObjProjectile();

	void move(GLfloat dt);
	void draw();

};

