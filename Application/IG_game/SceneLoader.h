#include <string>
#include <map>
#include <vector>
#include <GL/glut.h>
#include "assimp.h"
#include "aiPostProcess.h"
#include "aiScene.h"
#include "Animation.h"

#pragma once

using namespace std;

class SceneLoader
{
public:

	class Bone {
	public:
		aiMatrix4x4 Transformation;

		unsigned int ParentBoneId;
		bool hasParentBoneId;

		Bone() : hasParentBoneId(false) {}
	};

	const struct aiScene* Scene;
	map<string, GLuint> TextureIdMap;
	vector<Animation> Animations;
	vector<Bone> Bones;
	map<string, unsigned int> BoneName2boneId;

	SceneLoader(string path);
	~SceneLoader();
	unsigned int getBoneId(const aiNode* node); // Associa al nome di un bone un identificatore (tenendone traccia in BoneName2boneId)
	void createFrame(unsigned int animationId, double time, bool loop = true);

private:
	void loadScene(string path);
	void loadTextures();
	void loadAnimations();
	aiMatrix4x4 aiMatrix4x4Compose(const aiVector3D& scaling, const aiQuaternion& rotation, const aiVector3D& position);

};

