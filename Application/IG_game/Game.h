#include "Camera.h"
#include "GameObject.h"
#include "GameObjCircularBB.h"
#include "GameObjIntWall.h"
#include "GameObjStrWall.h"
#include "GameObjProjectile.h"
#include "GameObjBB.h"
#include <vector>
#include <map>
#include <list>
#include <GL/glut.h>
#include "GameObjBB_SkeletalAnimation.h"

#pragma once
using namespace std;
enum GameState {
	GAME_MENU,
	GAME_INTELLIGENCE_INTRO_1,
	GAME_INTELLIGENCE_INTRO_2,
	GAME_INTELLIGENCE_INTRO_3,
	GAME_INTELLIGENCE,
	GAME_INTELLIGENCE_END_1,
	GAME_INTELLIGENCE_END_2,
	GAME_INTELLIGENCE_END_3,
	GAME_INTELLIGENCE_PAUSE,
	GAME_STRENGTH_INTRO_1,
	GAME_STRENGTH_INTRO_2,
	GAME_STRENGTH_INTRO_3,
	GAME_STRENGTH,
	GAME_STRENGTH_END_1,
	GAME_STRENGTH_END_2,
	GAME_STRENGTH_END_3,
	GAME_STRENGTH_DEFEAT,
	GAME_STRENGTH_PAUSE
};

class Game
{
public:
	GameState State; // Stato corrente del gioco
	GLboolean Keys[1024]; // Vettore dei tasti premuti
	GLboolean LMouse; // Booleano che registra il clic sinistro del mouse 
	GLint LMouseX, LMouseY; // Contengono la posizione del mouse durante l'ultimo clic sinistro
	GLuint Width, Height; // Dimensioni dello schermo

	//Variabili relative a GAME_INTELLIGENCE
	const struct aiScene * IntScene; // Scena Intelligenza
	vector<GameObjBB*> IntCollision; // Vettore che contiene le statue con cui la telecamera deve collidere
	vector<GameObject*> IntDragonEggs; // Vettore che contiene le uova dei draghi nei due stati (accese/spente) 
	map<string, GLuint> IntTextureIDMap;
	GameObjIntWall *IntRoom; // Gestisce il render della stanza e la collisione con i muri
	GameObjBB *IntEntranceDoorL, *IntEntranceDoorR;
	GameObjBB *IntExitDoorL, *IntExitDoorR;
	GameObject *IntFire;
	GameObjBB_SkeletalAnimation *IntDragonAnimated; // Mesh della statua di drago animata
	GLboolean IntOpenEntranceDoors; // Booleano che indica che le porte d'ingresso hanno ricevuto l'input di aprirsi
	GLboolean IntRenderInteraction; // Booleano che indica alla funzione Render di renderizzare la scritta di interazione
	GLboolean IntInteractionTimeout; // Booleano che impedisce ulteriori interazioni per un tempo di timeout (Settato a true dopo aver premuto il tasto interazione, dopo un timeout viene settato a false)
	GLfloat IntInteractionTimeoutAcc; // Accumulatore del tempo di timeout per l'interazione
	GLint IntEnableCounter; // Numero di statue enabled

	//Variabili relative a GAME_STRENGTH
	const struct aiScene * StrScene; // Scena Forza
	list<GameObjProjectile*> StrAxes; // Lista che contiene le asce da lancio attualmente in volo
	vector<GameObjBB*> StrGolems; // Vettore che contiene i golem da distruggere
	map<string, GLuint> StrTextureIDMap;
	GameObjStrWall *StrWall; // Muro che delimita il movimento della telecamera
	GameObjStrWall *StrWallRoom; // Muro che delimita la stanza
	GameObject *StrGolemModel; // Oggetto di riferimento per la creazione dei golem
	GameObject *StrAxeModel; // Oggetto di riferimento per la creazione delle asce
	GameObject *StrRoom; // Contiene la mesh della stanza
	GameObject *StrLava; // Contiene la mesh della lava e del fuoco
	GameObjBB *StrEntranceDoor;
	GameObjBB *StrExitDoorL, *StrExitDoorR;
	GLboolean StrOpenEntranceDoor; // Booleano che indica che la porta d'ingresso ha ricevuto l'input di aprirsi
	GLboolean StrInteractionTimeout; // Booleano che impedisce ulteriori interazioni (creazione proiettile) per un tempo di timeout (Settato a true dopo aver premuto il tasto interazione, dopo un timeout viene settato a false)
	GLfloat StrInteractionTimeoutAcc; // Accumulatore del tempo di timeout per l'interazione
	vector<vec3> StrSpawnPosition; // Vettore delle posizioni iniziali di creazione dei golem
	GLint StrGolemNum; // Numero totale di golem creati
	GLfloat StrSpawnTimeoutAcc; // Accumulatore del tempo di timeout per l'attivazione di un nuovo golem
	GLint StrGolemActivationIndex; // Indice che indica il prossimo golem da attivare
	GLint StrLives; // Numero di vite
	GLint StrScore; // Punteggio / Numero di golem abbattuti

	Game(GLuint width, GLuint height);
	~Game();

	// Funzioni utili per il game loop
	void Init();
	void MenuInit();
	void StrInit();
	void IntInit();
	//void ProcessInput(GLfloat dt);
	void Update(GLfloat dt);
	void Render();
	void RenderString(GLdouble x, GLdouble y, const std::string &string, GLfloat red, GLfloat green, GLfloat blue);
	void RenderImage(GLuint texture);
	void mouseMove(int x, int y); // Gestione asincrona del movimento del mouse

};

