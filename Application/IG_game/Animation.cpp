#include "Animation.h"



Animation::Animation()
{
}


Animation::~Animation()
{
}

aiVector3D Animation::interpolate(const std::vector<aiVectorKey>& keys, double time, bool loop)
{
	time *= TicksPerSecond;

	if (loop) {
		time = fmod(time, Duration);
	}
	else if (time >= Duration) {
		return keys[keys.size() - 1].mValue;
	}

	unsigned int keyBefore = 0, keyAfter = 0;
	double frameDuration = 1.0, frameTime = 0.0;
	for (unsigned int cv = 0; cv<keys.size(); cv++) {
		if (time<keys[cv].mTime) {
			keyAfter = cv;
			if (cv == 0) {
				keyBefore = keys.size() - 1;
				frameDuration = keys[0].mTime;
				frameTime = time;
			}
			else {
				keyBefore = cv - 1;
				frameDuration = keys[keyAfter].mTime - keys[keyBefore].mTime;
				frameTime = time - keys[keyBefore].mTime;
			}
			break;
		}
	}

	double factor = frameTime / frameDuration;

	return interpolateFunction(keys[keyBefore].mValue, keys[keyAfter].mValue, factor);
}

aiVector3D Animation::interpolateFunction(const aiVector3D & beforeMatrix, const aiVector3D & afterMatrix, double factor)
{
	aiVector3D diffMatrix = afterMatrix - beforeMatrix;
	diffMatrix *= factor;

	return beforeMatrix + diffMatrix;
}

aiQuaternion Animation::interpolate(const vector<aiQuatKey>& keys, double time, bool loop)
{
	time *= TicksPerSecond;

	if (loop) {
		time = fmod(time, Duration);
	}
	else if (time >= Duration) {
		return keys[keys.size() - 1].mValue;
	}

	unsigned int keyBefore = 0, keyAfter = 0;
	double frameDuration = 1.0, frameTime = 0.0;
	for (unsigned int cv = 0; cv<keys.size(); cv++) {
		if (time<keys[cv].mTime) {
			keyAfter = cv;
			if (cv == 0) {
				keyBefore = keys.size() - 1;
				frameDuration = keys[0].mTime;
				frameTime = time;
			}
			else {
				keyBefore = cv - 1;
				frameDuration = keys[keyAfter].mTime - keys[keyBefore].mTime;
				frameTime = time - keys[keyBefore].mTime;
			}
			break;
		}
	}

	double factor = frameTime / frameDuration;

	return interpolateFunction(keys[keyBefore].mValue, keys[keyAfter].mValue, factor);
}

aiQuaternion Animation::interpolateFunction(const aiQuaternion & beforeMatrix, const aiQuaternion & afterMatrix, double factor)
{
	aiQuaternion returnMatrix;
	aiQuaternion::Interpolate(returnMatrix, beforeMatrix, afterMatrix, factor);

	return returnMatrix;
}