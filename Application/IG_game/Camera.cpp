#include "Camera.h"


void Camera::move(Camera_Mov direction, GLfloat dt)
{
	GLfloat velocity = MovementSpeed * dt;
	if (direction == FORWARD)
		Position += Front * velocity;
	if (direction == BACKWARD)
		Position -= Front * velocity;
	if (direction == LEFT)
		Position -= normalize(cross(Front, Up)) * velocity;
	if (direction == RIGHT)
		Position += normalize(cross(Front, Up)) * velocity;
	Position.y = HEIGHT;
}

void Camera::move(vec3 position)
{
	Position = position;
}

void Camera::rotateInt(GLfloat x, GLfloat y)
{
	Yaw += (x * MouseSensitivity);
	Pitch += (y * MouseSensitivity);

	if (Yaw < -270.0f) {
		Yaw += 360.0f;
	}
	else if (Yaw > 90.0f) {
		Yaw -= 360.0f;
	}

	if (Pitch > 89.0f)
		Pitch = 89.0f;
	else if (Pitch < -89.0f)
		Pitch = -89.0f;

	updateFrontVector();
}

void Camera::rotateStr(GLfloat x, GLfloat y)
{
	Yaw += (x * MouseSensitivity);
	Pitch += (y * MouseSensitivity);

	if (Yaw < -270.0f) {
		Yaw += 360.0f;
	}
	else if (Yaw > 90.0f) {
		Yaw -= 360.0f;
	}


	if (Pitch > 89.0f)
		Pitch = 89.0f;
	else if (Pitch < -89.0f)
		Pitch = -89.0f;

	updateFrontVector();
}

vec3 Camera::testPos(Camera_Mov direction, GLfloat dt)
{
	vec3 tempPos = Position;
	GLfloat velocity = MovementSpeed * dt;
	if (direction == FORWARD)
		tempPos += Front * velocity;
	if (direction == BACKWARD)
		tempPos -= Front * velocity;
	if (direction == LEFT)
		tempPos -= normalize(cross(Front, Up)) * velocity;
	if (direction == RIGHT)
		tempPos += normalize(cross(Front, Up)) * velocity;
	tempPos.y = HEIGHT;
	return (tempPos-Position) ;
}

vec3 Camera::testPos(vec3 direction, GLfloat dt)
{
	vec3 tempPos = Position;
	GLfloat velocity = MovementSpeed * dt;
	tempPos += direction*velocity;
	tempPos.y = HEIGHT;
	return (tempPos - Position);
}

void Camera::setDestination(vec3 destination)
{
	Destination = destination;
}

GLboolean Camera::testDestination()
{
	return (Position == Destination);
}

void Camera::moveTowardsDestination(GLfloat dt)
{
	if (Destination == Position) {
		return;
	}
	vec3 direction = normalize(Destination-Position);
	vec3 tempPos = Position;
	GLfloat velocity = MovementSpeed * dt;
	tempPos += direction*velocity;

	if (distance(tempPos,Position) > distance(Destination,Position)) 
	{ //se la distanza tra tempPos e Pos � pi� grande della distanza tra Destinatione e Pos, Setta la Pos a Destination
		Position = Destination;
	}
	// altrimenti setta la pos a tempPos
	else
	{
		Position = tempPos;
	}
}

void Camera::setDestinationYaw(GLfloat destinationYaw)
{
	DestinationYaw = destinationYaw;
}

GLboolean Camera::testDestinationYaw()
{
	return (Yaw == DestinationYaw);
}

void Camera::rotateTowardsDestinationYaw(GLfloat dt)
{
	if (DestinationYaw == Yaw) {
		return;
	}
	GLfloat tempYaw = Yaw;
	GLfloat velocity = RotationSpeed * dt;

	//la rotazione deve avvenire verso l'angolo destinazione
	if (DestinationYaw > Yaw)
	{
		tempYaw += velocity;
	}
	else if (DestinationYaw < Yaw)
	{
		tempYaw -= velocity;
	}

	if (abs(tempYaw - Yaw) > abs(DestinationYaw - Yaw))
	{ //se la distanza tra tempYaw e Yaw � pi� grande della distanza tra DestinationYaw e Yaw, setta la Yaw a DestinationYaw
		Yaw = DestinationYaw;
	}
	// altrimenti setta la Yaw a tempYaw
	else
	{
		Yaw = tempYaw;
	}
	updateFrontVector();
}

void Camera::setDestinationPitch(GLfloat destinationPitch)
{
	DestinationPitch = destinationPitch;
}

GLboolean Camera::testDestinationPitch()
{
	return (Pitch == DestinationPitch);
}

void Camera::rotateTowardsDestinationPitch(GLfloat dt)
{
	if (DestinationPitch == Pitch) {
		return;
	}

	GLfloat tempPitch = Pitch;
	GLfloat velocity = RotationSpeed * dt;

	//la rotazione deve avvenire verso l'angolo destinazione
	if (DestinationPitch > Pitch)
	{
		tempPitch += velocity;
	}
	else if (DestinationPitch < Pitch)
	{
		tempPitch -= velocity;
	}

	if (abs(tempPitch - Pitch) > abs(DestinationPitch - Pitch))
	{ //se la distanza tra tempPitch e Pitch � pi� grande della distanza tra DestinationPitch e Pitch, setta la Pitch a DestinationPitch
		Pitch = DestinationPitch;
	}
	// altrimenti setta la Pitch a tempPitch
	else
	{
		Pitch = tempPitch;
	}
	updateFrontVector();
}

Camera::Camera(vec3 position, vec3 up, GLfloat yaw, GLfloat pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), RotationSpeed(ROTSPEED)
{
	Position = position;
	Up = up;
	Yaw = yaw;
	Pitch = pitch;
	updateFrontVector();
	DestinationYaw = yaw;
}

Camera::~Camera()
{
}

void Camera::updateFrontVector()
{
	vec3 front;
	front.x = cos(radians(Pitch)) * cos(radians(Yaw));
	front.y = sin(radians(Pitch));
	front.z = cos(radians(Pitch)) * sin(radians(Yaw));
	Front = normalize(front);
}
