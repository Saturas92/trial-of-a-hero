#include <string>
#include <GL/glut.h>
#include "irrKlang.h"

#pragma once

using namespace std;
using namespace irrklang;

class SoundHandler
{
public:
	ISoundEngine* Engine;
	string Name;
	GLfloat Timeout;
	GLfloat Volume;
	GLfloat Accumulator;
	ISound* Sound;

	SoundHandler(ISoundEngine *engine, string name, GLfloat timeout, GLfloat volume);
	~SoundHandler();
	void play();
	void update(GLfloat dt);
};

