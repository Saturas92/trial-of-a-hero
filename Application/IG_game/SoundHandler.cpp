#include "SoundHandler.h"



SoundHandler::SoundHandler(ISoundEngine * engine, string name, GLfloat timeout, GLfloat volume) 
	: Engine(engine), Name(name), Timeout(timeout), Volume(volume), Accumulator(timeout+1)
{
}

SoundHandler::~SoundHandler()
{
}

void SoundHandler::play()
{
	if (Accumulator > Timeout) {
		Sound = Engine->play2D(Name.c_str(), GL_FALSE, GL_TRUE, GL_TRUE);
		Sound->setVolume(Volume);
		Sound->setIsPaused(false);

		Accumulator = 0;
	}
}

void SoundHandler::update(GLfloat dt)
{
	Accumulator += dt;
}
