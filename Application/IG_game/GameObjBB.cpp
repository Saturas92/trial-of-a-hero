#include "GameObjBB.h"
#include <limits>
#include <algorithm>
#include <cmath>
#include <iostream>

GameObjBB::GameObjBB(aiNode * meshEnable, const aiScene * scene, map<string, GLuint> textureIDMap, vec3 rotationCenter, aiNode * meshDisable, ObjectState state, GLboolean render, GLfloat speed, GLfloat rotationSpeed)
	:GameObject(meshEnable, scene, textureIDMap, meshDisable, state, render)
{
	xMax = std::numeric_limits<float>::lowest();
	zMax = std::numeric_limits<float>::lowest();
	yMax = std::numeric_limits<float>::lowest();
	xMin = std::numeric_limits<float>::max();
	zMin = std::numeric_limits<float>::max();
	yMin = std::numeric_limits<float>::max();
	Position = vec3(0, 0, 0);
	GLfloat count = 0;
	recursiveCount(Scene, MeshEnable, &xMax, &zMax, &yMax, &xMin, &zMin, &yMin,&count);
	Position.x /= count;
	Position.y /= count;
	Position.z /= count;
	OriginalPosition = Position;
	Yaw = 0;
	OriginalYaw = 0;
	MovementSpeed = speed;
	RotationSpeed = rotationSpeed;
	RotationCenter = rotationCenter;
	TimePassed = 0;
}

GameObjBB::~GameObjBB()
{
}

vec3 GameObjBB::collidesWith(vec3 source,vec3 destination)
{
	//if (point.y<yMin || point.y>yMax || point.x<(xMin) || point.x>(xMax) || point.z<(zMin) || point.z>(zMax))
	//{
	//	return GL_FALSE;
	//}
    //return GL_TRUE;
	//per tener conto dello spostamento dell'oggetto, sommo lo spostamento a source e destination
	if (Position != OriginalPosition) {
		source -= (Position - OriginalPosition);
		destination -= (Position - OriginalPosition);
	}
	if (destination.y<yMin || destination.y>yMax || destination.x<(xMin) || destination.x>(xMax) || destination.z<(zMin) || destination.z>(zMax))
	{
		return vec3(0, 0, 0);
	}
	if (source.z <= zMax&&source.z >= zMin)
	{
		if (source.x > xMax)
		{
			return normalize(vec3(0, 0, zMax - zMin));
		}
		else if(source.x < xMin)
		{
			return normalize(vec3(0, 0, -zMax + zMin));
		}
	}

	if (source.x <= xMax&&source.x >= xMin)
	{
		if (source.z > zMax)
		{
			return normalize(vec3(-xMax + xMin, 0,0 ));
		}
		else if (source.z < zMin)
		{
			return normalize(vec3(xMax - xMin, 0, 0));
		}
	}

	if (source.x > xMax)
	{
		if (source.z > zMax)
		{
			if ((source.z + ((destination.z - source.z) / (destination.x - source.x))*(xMax - source.x)) <= zMax)
			{
				//ritorno direzione verso l alto
				return normalize(vec3(0, 0, zMax - zMin));
			}
			else
			{
				return normalize(vec3(xMin-xMax, 0, 0));
			}

		}
		if (source.z < zMin)
		{
			if ((source.z + ((destination.z - source.z) / (destination.x - source.x))*(xMax - source.x)) >= zMin)
			{
				//ritorno direzione verso l alto
				return normalize(vec3(0, 0, zMax - zMin));
			}
			else
			{
				return normalize(vec3(xMax - xMin, 0, 0));
			}
		}
	}

	if (source.x < xMin)
	{
		if (source.z > zMax)
		{
			if ((source.z + ((destination.z - source.z) / (destination.x - source.x))*(xMax - source.x)) <= zMax)
			{
				//ritorno direzione verso il basso
				return normalize(vec3(0, 0, zMin - zMax));
			}
			else
			{
				//sinistra
				return normalize(vec3(xMin - xMax, 0, 0));
			}

		}
		if (source.z < zMin)
		{
			if ((source.z + ((destination.z - source.z) / (destination.x - source.x))*(xMax - source.x)) >= zMin)
			{
				//ritorno direzione verso il basso
				return normalize(vec3(0, 0, zMin - zMax));
			}
			else
			{
				//destra
				return normalize(vec3(xMax - xMin, 0, 0));
			}
		}
	}
}

void GameObjBB::draw()
{
	glPushMatrix();
	if (Position != OriginalPosition) {
		vec3 tempPos = Position - OriginalPosition;
		glTranslatef(tempPos.x, tempPos.y, tempPos.z);
	}
	if (Yaw != OriginalYaw) {
		//Per ruotare attorno a (Position + RotationCenter), trasliamo nell'origine, ruotiamo rispetto all'asse y e poi ritrasliamo nel punto originale
		vec3 translate = (Position + RotationCenter);
		glTranslatef(translate.x, translate.y, translate.z);
		glRotatef(Yaw, 0.0f, 1.0f, 0.0f);
		glTranslatef(-translate.x, -translate.y, -translate.z);
	}
	GameObject::draw();
	glPopMatrix();

}

void GameObjBB::setDestination(vec3 destination)
{
	Destination = destination;
}

GLboolean GameObjBB::testDestination()
{
	return (Position == Destination);
}

void GameObjBB::moveTowardsDestination(GLfloat dt)
{
	if (Destination == Position) {
		return;
	}
	vec3 direction = normalize(Destination - Position);
	vec3 tempPos = Position;
	GLfloat velocity = MovementSpeed * dt;
	tempPos += direction*velocity;

	if (distance(tempPos, Position) > distance(Destination, Position))
	{ //se la distanza tra tempPos e Pos � pi� grande della distanza tra Destinatione e Pos, Setta la Pos a Destination
		Position = Destination;
	}
	// altrimenti setta la pos a tempPos
	else
	{
		Position = tempPos;
	}
}

void GameObjBB::setDestinationYaw(GLfloat destinationYaw)
{
	DestinationYaw = destinationYaw;
}

GLboolean GameObjBB::testDestinationYaw()
{
	return (Yaw == DestinationYaw);
}

void GameObjBB::rotateTowardsDestinationYaw(GLfloat dt)
{
	if (DestinationYaw == Yaw) {
		return;
	}

	GLfloat tempYaw = Yaw;
	GLfloat velocity = RotationSpeed * dt;

	//la rotazione deve avvenire verso l'angolo destinazione
	if (DestinationYaw > Yaw)
	{
		tempYaw += velocity;
	}
	else if (DestinationYaw < Yaw)
	{
		tempYaw -= velocity;
	}

	if (abs(tempYaw - Yaw) > abs(DestinationYaw - Yaw))
	{ //se la distanza tra tempYaw e Yaw � pi� grande della distanza tra DestinationYaw e Yaw, setta la Yaw a DestinationYaw
		Yaw = DestinationYaw;
	}
	// altrimenti setta la Yaw a tempYaw
	else
	{
		Yaw = tempYaw;
	}
}

void GameObjBB::DisableRender(GLfloat dt)
{
	TimePassed += dt;
	if (TimePassed >= 1.5) {
		Render = GL_FALSE;
	}
}

void GameObjBB::recursiveCount(const aiScene * scene, aiNode * node, GLfloat * xMax, GLfloat * zMax, GLfloat * yMax, GLfloat * xMin, GLfloat * zMin, GLfloat * yMin, GLfloat * countNumVertices)
{
	for (unsigned int n = 0; n < node->mNumMeshes; ++n)
	{
		const struct aiMesh* mesh = scene->mMeshes[node->mMeshes[n]];
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			Position.x += mesh->mVertices[i].x;
			Position.y += mesh->mVertices[i].y;
			Position.z += mesh->mVertices[i].z;
			(*countNumVertices)++;

			if (mesh->mVertices[i].x>(*xMax))
				*xMax = mesh->mVertices[i].x;
			if (mesh->mVertices[i].z>(*zMax))
				*zMax = mesh->mVertices[i].z;
			if (mesh->mVertices[i].y>(*yMax))
				*yMax = mesh->mVertices[i].y;
			if (mesh->mVertices[i].x < (*xMin))
				*xMin = mesh->mVertices[i].x;
			if (mesh->mVertices[i].z < (*zMin))
				*zMin = mesh->mVertices[i].z;
			if (mesh->mVertices[i].y < (*yMin))
				*yMin = mesh->mVertices[i].y;

		}
	}
		for (unsigned int n = 0; n < node->mNumChildren; ++n)
		{
			recursiveCount(scene, node->mChildren[n], xMax, zMax, yMax, xMin, zMin, yMin, countNumVertices);
		}

}

