#include "SceneLoader.h"
#include <IL\il.h>


SceneLoader::SceneLoader(string path)
{
	//Carica la scena
	loadScene(path);

	//Carica le texture
	loadTextures();

	//Carica le animazioni
	loadAnimations();
}


SceneLoader::~SceneLoader()
{
}

void SceneLoader::loadScene(string path)
{
	Scene = aiImportFile(path.c_str(), aiProcessPreset_TargetRealtime_Quality);
}

void SceneLoader::loadTextures()
{
	ILboolean success;

	// getTexture Filenames and Number of Textures
	for (unsigned int m = 0; m < Scene->mNumMaterials; m++)
	{
		int texIndex = 0;
		aiReturn texFound = AI_SUCCESS;

		aiString path;	// filename

		while (texFound == AI_SUCCESS)
		{
			texFound = Scene->mMaterials[m]->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
			TextureIdMap[path.data] = 0; //fill map with textures, pointers still NULL yet
			texIndex++;
		}
	}

	int numTextures = TextureIdMap.size();

	/* array with DevIL image IDs */
	ILuint* imageIds = new ILuint[numTextures];
	/* generate DevIL Image IDs */
	ilGenImages(numTextures, imageIds); /* Generation of numTextures image names */

	/* create and fill array with GL texture ids */
	GLuint*	textureIds;
	textureIds = new GLuint[numTextures];
	glGenTextures(numTextures, textureIds); /* Texture name generation */

	/* get iterator */
	map<string, GLuint>::iterator itr = TextureIdMap.begin();

	for (int i = 0; i < numTextures; i++)
	{

		//save IL image ID
		string filename = (*itr).first;  // get filename
		(*itr).second = textureIds[i];	  // save texture id for filename in map
		itr++;								  // next texture


		ilBindImage(imageIds[i]); /* Binding of DevIL image name */
		string fileloc = "./models/" + filename;	/* Loading of image */
		success = ilLoadImage(fileloc.c_str());

		//fprintf(stdout, "Loading Image: %s\n", fileloc.data());

		if (success) /* If no error occured: */
		{
			success = ilConvertImage(IL_RGBA, IL_UNSIGNED_BYTE); /* Convert every colour component into
																unsigned byte. If your image contains alpha channel you can replace IL_RGB with IL_RGBA */
			if (!success)
			{
				/* Error occured */
				fprintf(stderr, "Couldn't convert image");
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, textureIds[i]); /* Binding of texture name */

				//redefine standard texture values
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); /* We will use linear interpolation for magnification filter */
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); /* We will use linear interpolation for minifying filter */

				glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_BPP), ilGetInteger(IL_IMAGE_WIDTH),
					ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), GL_UNSIGNED_BYTE,
					ilGetData()); /* Texture specification */
			}
		}
		else
		{
			/* Error occured */
			fprintf(stderr, "Couldn't load Image: %s\n", fileloc.data());
		}
	}
	ilDeleteImages(numTextures, imageIds); /* Because we have already copied image data into texture data
										   we can release memory used by image. */

										   //Cleanup
	delete[] imageIds;
	imageIds = NULL;

}

void SceneLoader::loadAnimations()
{
	// Trova e salva per ogni animazione i canali (e le rispettive ossa)
	for (int a = 0; a < Scene->mNumAnimations; a++) {
		Animation animation;

		animation.Duration = Scene->mAnimations[a]->mDuration;
		animation.TicksPerSecond = Scene->mAnimations[a]->mTicksPerSecond;

		animation.Channels.resize(Scene->mAnimations[a]->mNumChannels);
		for (unsigned int c = 0; c < Scene->mAnimations[a]->mNumChannels; c++) {
			animation.Channels[c].Positions.resize(Scene->mAnimations[a]->mChannels[c]->mNumPositionKeys);
			animation.Channels[c].Scales.resize(Scene->mAnimations[a]->mChannels[c]->mNumScalingKeys);
			animation.Channels[c].Rotations.resize(Scene->mAnimations[a]->mChannels[c]->mNumRotationKeys);

			for (unsigned int p = 0; p < Scene->mAnimations[a]->mChannels[c]->mNumPositionKeys; p++) {
				animation.Channels[c].Positions[p] = Scene->mAnimations[a]->mChannels[c]->mPositionKeys[p];
			}
			for (unsigned int s = 0; s < Scene->mAnimations[a]->mChannels[c]->mNumScalingKeys; s++) {
				animation.Channels[c].Scales[s] = Scene->mAnimations[a]->mChannels[c]->mScalingKeys[s];
			}
			for (unsigned int r = 0; r < Scene->mAnimations[a]->mChannels[c]->mNumRotationKeys; r++) {
				animation.Channels[c].Rotations[r] = Scene->mAnimations[a]->mChannels[c]->mRotationKeys[r];
			}

			const aiNode* node = Scene->mRootNode->FindNode(Scene->mAnimations[a]->mChannels[c]->mNodeName);
			animation.Channels[c].BoneId = getBoneId(node);
		}

		Animations.push_back(animation);
	}

	//Find all the bones, and their parent bones, connected to the meshes
	//Scorre le mesh, per ogni mesh scorre le bones, per ogni bone risale l'albero di parentela per assegnare i Bone::parentBoneId
	for (unsigned int m = 0; m < Scene->mNumMeshes; m++) {
		for (unsigned int b = 0; b < Scene->mMeshes[m]->mNumBones; b++) {
			const aiNode* node = Scene->mRootNode->FindNode(Scene->mMeshes[m]->mBones[b]->mName);
			unsigned int boneId = getBoneId(node);

			if (!Bones[boneId].hasParentBoneId) {
				//Popola i Bone::parentBoneId
				node = node->mParent;
				while (node != Scene->mRootNode) {
					unsigned int parentBoneId = getBoneId(node);
					Bones[boneId].ParentBoneId = parentBoneId;
					Bones[boneId].hasParentBoneId = true;
					boneId = parentBoneId;

					node = node->mParent;
				}
			}
		}
	}


}

unsigned int SceneLoader::getBoneId(const aiNode * node)
{
	unsigned int boneId;
	if (BoneName2boneId.count(node->mName.data) == 0) { // Se il bone non � gi� nella mappa BoneName2boneId
		// gli assegno un id
		boneId = Bones.size();
		BoneName2boneId[node->mName.data] = boneId;

		// creo un oggetto bone in cui mi salvo la sua transformation attuale
		Bone b;
		b.Transformation = node->mTransformation;

		// lo inserisco nel vettore di bone, all'indice id
		Bones.push_back(b);
	}
	else { // Altrimenti restituisco il suo id
		boneId = BoneName2boneId[node->mName.data];
	}

	return boneId;
}

void SceneLoader::createFrame(unsigned int animationId, double time, bool loop)
{
	if (animationId < Animations.size()) {
		for (auto& channel : Animations[animationId].Channels) {
			aiVector3D scale = Animations[animationId].interpolate(channel.Scales, time, loop);
			aiQuaternion rotation = Animations[animationId].interpolate(channel.Rotations, time, loop);
			aiVector3D position = Animations[animationId].interpolate(channel.Positions, time, loop);
			Bones[channel.BoneId].Transformation = aiMatrix4x4Compose(scale, rotation, position);
		}
	}

}

aiMatrix4x4 SceneLoader::aiMatrix4x4Compose(const aiVector3D& scaling, const aiQuaternion& rotation, const aiVector3D& position) {
	aiMatrix4x4 r;

	aiMatrix3x3 m = rotation.GetMatrix();

	r.a1 = m.a1 * scaling.x;
	r.a2 = m.a2 * scaling.x;
	r.a3 = m.a3 * scaling.x;
	r.a4 = position.x;

	r.b1 = m.b1 * scaling.y;
	r.b2 = m.b2 * scaling.y;
	r.b3 = m.b3 * scaling.y;
	r.b4 = position.y;

	r.c1 = m.c1 * scaling.z;
	r.c2 = m.c2 * scaling.z;
	r.c3 = m.c3 * scaling.z;
	r.c4 = position.z;

	r.d1 = 0.0;
	r.d2 = 0.0;
	r.d3 = 0.0;
	r.d4 = 1.0;

	return r;
}
