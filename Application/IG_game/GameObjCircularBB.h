#include <glm/glm/glm.hpp>
#include "GameObject.h"
#pragma once

using namespace glm;

// Classe per oggetti della scena con Bounding Box cilindrico
class GameObjCircularBB :
	public GameObject
{
public:
	vec3 Center;
	GLfloat Radius;
	GLfloat yMin, yMax, xMax, xMin, zMin, zMax;
	

	GameObjCircularBB(aiNode *mesh, const aiScene *scene, map<string, GLuint> textureIDMap);
	~GameObjCircularBB();
	GLboolean collidesWith(vec3 point);
private:
	void recursiveCount(const aiScene * scene, aiNode * node, GLfloat * count, GLfloat * xMax, GLfloat * zMax, GLfloat * yMax, GLfloat * xMin, GLfloat * zMin, GLfloat * yMin);
};

