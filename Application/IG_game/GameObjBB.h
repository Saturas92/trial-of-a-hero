#include <glm/glm/glm.hpp>
#include "GameObject.h"
#pragma once

using namespace glm;
const GLfloat SPEED_OBJ = 1.0f;
const GLfloat ROTSPEED_OBJ = 35.0f;

class GameObjBB :
	public GameObject
{
public:
	GLfloat yMin, yMax, xMax, xMin, zMin, zMax;
	GLfloat MovementSpeed;
	vec3 Position; // Media dei vertici della meshEnable
	vec3 Destination;
	vec3 OriginalPosition;
	vec3 RotationCenter; // Centro di rotazione, inteso come delta rispetto a Position

	GLfloat Yaw;
	GLfloat DestinationYaw;
	GLfloat OriginalYaw;
	GLfloat RotationSpeed;

	GameObjBB(aiNode *meshEnable, const aiScene *scene, map<string, GLuint> textureIDMap, vec3 rotationCenter = vec3(0, 0, 0), aiNode *meshDisable = NULL, ObjectState state = STATE_ENABLE, GLboolean render = GL_TRUE, GLfloat speed = (SPEED_OBJ), GLfloat rotationSpeed = ROTSPEED_OBJ);
	~GameObjBB();
	vec3 collidesWith(vec3 source,vec3 destination); // Ritorna vec3(0,0,0) in caso di non collisione
	void draw();

	//Funzioni per il movimento dell'oggetto
	void setDestination(vec3 destination);
	GLboolean testDestination();
    void moveTowardsDestination(GLfloat dt);

	//Funzioni per la rotazione dell'oggetto rispetto a (Position + RotationCenter)
	void setDestinationYaw(GLfloat destinationYaw);
	GLboolean testDestinationYaw();
	void rotateTowardsDestinationYaw(GLfloat dt);

	GLfloat TimePassed; // Variabile che viene aggiornata ad ogni chiamata di DisableRender
	void DisableRender(GLfloat dt); // Funzione che disabilita il render se la variabile TimePassed supera una determinata soglia

private:
	void recursiveCount(const aiScene * scene, aiNode * node, GLfloat * xMax, GLfloat * zMax, GLfloat * yMax, GLfloat * xMin, GLfloat * zMin, GLfloat * yMin,GLfloat* countNumVertices);
};

