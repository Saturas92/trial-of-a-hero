#pragma once
#include "aiScene.h"
#include <vector>

using namespace std;

class Animation
{
public:
	class Channel { // In ogni canale c'� un osso e i suoi dati su posizione, rotazione e scalamento
	public:
		unsigned int BoneId;

		vector<aiVectorKey> Positions;
		vector<aiQuatKey> Rotations;
		vector<aiVectorKey> Scales;
	};

	double Duration;
	double TicksPerSecond;

	std::vector<Channel> Channels;


	Animation();
	~Animation();
	aiVector3D interpolate(const vector<aiVectorKey>& keys, double time, bool loop = true);
	aiVector3D interpolateFunction(const aiVector3D& beforeMatrix, const aiVector3D& afterMatrix, double factor);
	aiQuaternion interpolate(const vector<aiQuatKey>& keys, double time, bool loop = true);
	aiQuaternion interpolateFunction(const aiQuaternion& beforeMatrix, const aiQuaternion& afterMatrix, double factor);
};

