#include "GameObjBB_SkeletalAnimation.h"
#include <glm\glm\gtx\transform.hpp>



GameObjBB_SkeletalAnimation::GameObjBB_SkeletalAnimation(aiNode * meshEnable, const aiScene * scene, map<string, GLuint> textureIDMap, SceneLoader * loader, unsigned int animationId, GLboolean loop, vec3 rotationCenter, aiNode * meshDisable, ObjectState state, GLboolean render, GLfloat speed, GLfloat rotationSpeed)
	: GameObjBB(meshEnable, scene, textureIDMap, rotationCenter, meshDisable, state, render, speed, rotationSpeed)
{
	Loader = loader;
	AnimationId = animationId;
	Time = 0;
	Loop = loop;

	//Scorre le mesh, per ogni mesh scorre le bones, per ogni bone salva i suoi weights
	Weights.resize(MeshEnable->mNumMeshes);
	for (unsigned int m = 0; m < MeshEnable->mNumMeshes; m++) {

		const struct aiMesh* mesh = Scene->mMeshes[MeshEnable->mMeshes[m]];

		Mesh tempmesh;
		for (unsigned int v = 0; v < mesh->mNumVertices; v++) {
			tempmesh.Vertices.push_back(mesh->mVertices[v]);
			tempmesh.Normals.push_back(mesh->mNormals[v]);
		}
		backupMeshes.push_back(tempmesh);


		for (unsigned int b = 0; b < mesh->mNumBones; b++) {
			
			BoneWeights bw;
			const aiNode* node = Scene->mRootNode->FindNode(mesh->mBones[b]->mName);
			unsigned int boneId = loader->getBoneId(node);
			bw.boneId = boneId;
			bw.offsetMatrix = mesh->mBones[b]->mOffsetMatrix;

			for (unsigned int w = 0; w < mesh->mBones[b]->mNumWeights; w++) {
				bw.weights.push_back(mesh->mBones[b]->mWeights[w]);
			}

			Weights[m].push_back(bw);
		}
	}

}


GameObjBB_SkeletalAnimation::~GameObjBB_SkeletalAnimation()
{
}

void GameObjBB_SkeletalAnimation::updateFrame(double dt)
{
	Time += dt;
	Loader->createFrame(AnimationId, Time, Loop);

	struct aiNode* node = MeshEnable;

	for (int m = 0; m < node->mNumMeshes; ++m) {
		const struct aiMesh* mesh = Scene->mMeshes[node->mMeshes[m]];

		for (unsigned int v = 0; v < mesh->mNumVertices; v++) {
			if (backupMeshes[m].Vertices[v] != mesh->mVertices[v])
				mesh->mVertices[v] = 0;
			if (backupMeshes[m].Normals[v] != mesh->mNormals[v])
				mesh->mNormals[v] = 0;
		}

		//Calculate new frame vertices and normals
		for (unsigned int cb = 0; cb < Weights[m].size(); cb++) { // Per ogni osso della mesh m
			const auto& boneWeights = Weights[m][cb];

			transformation = aiMatrix4x4();
			calculateBoneTransformation(boneWeights.boneId);
			transformation *= boneWeights.offsetMatrix;

			for (auto& weight : boneWeights.weights) {
				mesh->mVertices[weight.mVertexId] += weight.mWeight*(transformation*backupMeshes[m].Vertices[weight.mVertexId]);
				mesh->mNormals[weight.mVertexId] += weight.mWeight*(aiMatrix3x3(transformation)*backupMeshes[m].Normals[weight.mVertexId]);

			}

		}

	}

}

void GameObjBB_SkeletalAnimation::drawFrame()
{


	glPushMatrix();
	if (Position != OriginalPosition) {
		vec3 tempPos = Position - OriginalPosition;
		glTranslatef(tempPos.x, tempPos.y, tempPos.z);
	}
	if (Yaw != OriginalYaw) {
		//Per ruotare attorno a (Position + RotationCenter), trasliamo nell'origine, ruotiamo rispetto all'asse y e poi ritrasliamo nel punto originale
		vec3 translate = (Position + RotationCenter);
		glTranslatef(translate.x, translate.y, translate.z);
		glRotatef(Yaw, 0.0f, 1.0f, 0.0f);
		glTranslatef(-translate.x, -translate.y, -translate.z);
	}


	struct aiNode* node = MeshEnable;
	struct aiMatrix4x4 m = node->mTransformation;

	//float scale = 1.0f;

	//m.Scaling(aiVector3D(scale, scale, scale), m);

	// update transform
	m.Transpose();
	//glPushMatrix();
	glMultMatrixf((float*)&m);

	// draw all meshes assigned to this node
	for (int n = 0; n < node->mNumMeshes; ++n)
	{
		const struct aiMesh* mesh = Scene->mMeshes[node->mMeshes[n]];

		GameObject::applyMaterial(Scene->mMaterials[mesh->mMaterialIndex]);


		if (mesh->HasTextureCoords(0))
			glEnable(GL_TEXTURE_2D);
		else
			glDisable(GL_TEXTURE_2D);
		if (mesh->mNormals == NULL)
		{
			//cout << "No normals" << endl;
			glDisable(GL_LIGHTING);
		}
		else
		{
			glEnable(GL_LIGHTING);
		}

		if (mesh->mColors[0] != NULL)
		{
			glEnable(GL_COLOR_MATERIAL);
		}
		else
		{
			glDisable(GL_COLOR_MATERIAL);
		}



		for (int t = 0; t < mesh->mNumFaces; ++t) {
			const struct aiFace* face = &mesh->mFaces[t];
			GLenum face_mode;

			switch (face->mNumIndices)
			{
			case 1: face_mode = GL_POINTS; break;
			case 2: face_mode = GL_LINES; break;
			case 3: face_mode = GL_TRIANGLES; break;
			default: face_mode = GL_POLYGON; break;
			}

			glBegin(face_mode);

			for (int i = 0; i < face->mNumIndices; i++)		// go through all vertices in face
			{
				int vertexIndex = face->mIndices[i];	// get group index for current index
				if (mesh->mColors[0] != NULL)
					Color4f(&mesh->mColors[0][vertexIndex]);
				if (mesh->mNormals != NULL)

					if (mesh->HasTextureCoords(0))		//HasTextureCoords(texture_coordinates_set)
					{
						glTexCoord2f(mesh->mTextureCoords[0][vertexIndex].x, 1 - mesh->mTextureCoords[0][vertexIndex].y); //mTextureCoords[channel][vertex]
					}

				glNormal3fv(&mesh->mNormals[vertexIndex].x);
				glVertex3fv(&mesh->mVertices[vertexIndex].x);

			}

			glEnd();

		}

	}

	glPopMatrix();

	//glPopMatrix();
}

void GameObjBB_SkeletalAnimation::calculateBoneTransformation(unsigned int boneId) {
	if (Loader->Bones[boneId].hasParentBoneId) {
		calculateBoneTransformation(Loader->Bones[boneId].ParentBoneId);
	}
	transformation *= Loader->Bones[boneId].Transformation;

}

