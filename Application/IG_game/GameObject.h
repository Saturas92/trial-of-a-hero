#include "assimp.h"
#include "aiPostProcess.h"
#include "aiScene.h"
#include <GL/glut.h>
#include <IL/il.h>
#include <map>
#include <string>

using namespace std;
enum ObjectState {
	STATE_ENABLE,
	STATE_DISABLE
};

#pragma once
class GameObject
{
public:
	aiNode* MeshEnable;
	aiNode* MeshDisable;
	const aiScene* Scene;
	map<string, GLuint> TextureIDMap;
	ObjectState State;
	GLboolean Render;
	
	GameObject(aiNode *meshEnable, const aiScene *scene, map<string, GLuint> textureIDMap, aiNode *meshDisable = NULL, ObjectState state = STATE_ENABLE, GLboolean render = GL_TRUE);
	~GameObject();
	void draw();
protected: 
	void recursiveRender(const struct aiScene *scene, const struct aiNode* node);
	void Color4f(const struct aiColor4D *color);
	void applyMaterial(const struct aiMaterial *mtl);
	void color4ToFloat4(const struct aiColor4D *c, float f[4]);
	void setFloat4(float f[4], float a, float b, float c, float d);
	


};

