#include "GameObjStrWall.h"


GameObjStrWall::GameObjStrWall(aiNode * mesh, const aiScene * scene, map<string, GLuint> textureIDMap, GLfloat xmax, GLfloat xmin, GLfloat ymax, GLfloat ymin, GLfloat zmax, GLfloat zmin)
	: GameObject(mesh, scene, textureIDMap), Xmin(xmin), Xmax(xmax), Ymin(ymin), Ymax(ymax), Zmin(zmin), Zmax(zmax)
{
}

GameObjStrWall::~GameObjStrWall()
{
}

GLboolean GameObjStrWall::collidesWith(vec3 destination)
{
	if (destination.x < Xmin || destination.x > Xmax || destination.y < Ymin || destination.y > Ymax || destination.z < Zmin || destination.z > Zmax ) {
		return GL_TRUE;
	}
	return GL_FALSE;
}
