#include "GameObjCircularBB.h"
#include <limits>
#include <algorithm>
#include <cmath>
#include <iostream>


GameObjCircularBB::GameObjCircularBB(aiNode *mesh, const aiScene *scene, map<string, GLuint> textureIDMap):GameObject(mesh, scene, textureIDMap)
{
	Center = vec3(0, 0, 0);
	GLfloat count = 0;
	xMax = std::numeric_limits<float>::lowest();
	zMax = std::numeric_limits<float>::lowest();
	yMax = std::numeric_limits<float>::lowest();
	xMin = std::numeric_limits<float>::max();
	zMin = std::numeric_limits<float>::max();
	yMin = std::numeric_limits<float>::max();
	std::cout << "Radius:" << Radius << ", x:" << Center.x << ", y:" << Center.y << ", z:" << Center.z << ", xMin:" << xMin << ", xMax:" << xMax << ", yMin:" << yMin << ", yMax:" << yMax << ", zMin:" << zMin << ", zMax:" << zMax << std::endl;
	recursiveCount(Scene, MeshEnable, &count, &xMax, &zMax, &yMax, &xMin, &zMin, &yMin);
	Center.x /= count;
	Center.y /= count;
	Center.z /= count;
    Radius = std::max(std::max(std::abs(xMax - Center.x), std::abs(xMin - Center.x)), std::max(std::abs(zMax - Center.z), std::abs(zMin - Center.z)));
	std::cout << "Radius:" << Radius << ", x:" << Center.x << ", y:" << Center.y << ", z:" << Center.z << ", xMin:" << xMin << ", xMax:" << xMax << ", yMin:" << yMin << ", yMax:" << yMax <<  ", zMin:" << zMin << ", zMax:" << zMax << std::endl;
}


GameObjCircularBB::~GameObjCircularBB()
{
}

GLboolean GameObjCircularBB::collidesWith(vec3 point)
{
	if (point.y<yMin || point.y>yMax || point.x<(xMin-0.5) || point.x>(xMax + 0.5) || point.z<(zMin - 0.5) || point.z>(zMax + 0.5))
	{
		return GL_FALSE;
	}
	
	return ((pow((point.x-Center.x),2)-pow((point.z-Center.z),2))<pow(Radius+0.5,2));
}

void GameObjCircularBB::recursiveCount(const aiScene * scene, aiNode * node, GLfloat * count, GLfloat * xMax, GLfloat * zMax, GLfloat * yMax, GLfloat * xMin, GLfloat * zMin, GLfloat * yMin )
{
	for (unsigned int n = 0; n < node->mNumMeshes; ++n)
	{
		const struct aiMesh* mesh = scene->mMeshes[node->mMeshes[n]];
		for (unsigned int i = 0; i < mesh->mNumVertices; i++)
		{
			Center.x += mesh->mVertices[i].x;
			Center.y += mesh->mVertices[i].y;
			Center.z += mesh->mVertices[i].z;
			if (mesh->mVertices[i].x>(*xMax))
				*xMax = mesh->mVertices[i].x;
			if (mesh->mVertices[i].z>(*zMax))
				*zMax = mesh->mVertices[i].z;
			if (mesh->mVertices[i].y>(*yMax))
				*yMax = mesh->mVertices[i].y;
			if (mesh->mVertices[i].x < (*xMin))
				*xMin = mesh->mVertices[i].x;
			if (mesh->mVertices[i].z < (*zMin))
				*zMin = mesh->mVertices[i].z;
			if (mesh->mVertices[i].y < (*yMin))
				*yMin = mesh->mVertices[i].y;
			(*count)++;
		}
	}

	for (unsigned int n = 0; n < node->mNumChildren; ++n)
	{
		recursiveCount(scene, node->mChildren[n], count, xMax, zMax,yMax, xMin, zMin,yMin );
	}
}