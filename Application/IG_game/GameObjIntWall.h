#pragma once
#include "GameObject.h"
#include <glm/glm/glm.hpp>

using namespace glm;
class GameObjIntWall :
	public GameObject
{
public:
	GameObjIntWall(aiNode *mesh, const aiScene *scene, map<string, GLuint> textureIDMap);
	vec3 collidesWith(vec3 source,vec3 destination);


	~GameObjIntWall();
};

