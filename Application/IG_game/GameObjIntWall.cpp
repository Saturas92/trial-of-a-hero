#include "GameObjIntWall.h"



GameObjIntWall::GameObjIntWall(aiNode *mesh, const aiScene *scene, map<string, GLuint> textureIDMap):GameObject(mesh, scene, textureIDMap)
{
}

vec3 GameObjIntWall::collidesWith(vec3 source, vec3 destination)
{
	if ((destination.x >= (-2.31018) && destination.x <= (2.31018) && destination.z >= (-5.57728) && destination.z <= (5.57728))
		|| (destination.x >= (-5.57728) && destination.x <= (-2.31018) && destination.z >= (-2.31018) && destination.z <= (2.31018))
		|| (destination.x >= (2.31018) && destination.x <= (5.57728) && destination.z >= (-2.31018) && destination.z <= (2.31018)))
	{
		return vec3(0, 0, 0);
	}
	else if (destination.x >= (-2.31018) && destination.x <= (2.31018))
	{
		if (destination.z > 5.57728)
		{
			return normalize(vec3(-4.62036, 0, 0)); //sinistra
		}
		if (destination.z < -5.57728)
		{
			return normalize(vec3(4.62036, 0, 0)); //destra
		}
	}
	else if (destination.z >= (-2.31018) && destination.z <= (2.31018))
	{
		if (destination.x > 5.57728)
		{
			return normalize(vec3(0, 0, 4.62036)); //alto
		}
		if (destination.x < -5.57728)
		{
			return normalize(vec3(0, 0, -4.62036)); //basso
		}
	}
	else if (destination.x > (2.31018))
	{
		if (destination.z > 2.31018)
		{
			if (destination.z + destination.x - 7.88746 < 0)
			{
				return vec3(0, 0, 0);
			}
			else
			{
				return normalize(vec3(-3.2671, 0, 3.2671)); //alto a sinistra
			}
			   
		}
		if (destination.z < -2.31018)
		{
			if (destination.z - destination.x + 7.88746 > 0)
			{
				return vec3(0, 0, 0);
			}
			else
			{
				return normalize(vec3(3.2671, 0, 3.2671)); //alto a destra
			}
		}
		
	}
	else if (destination.x <(-2.31018))
	{
		if (destination.z > 2.31018)
		{
			if (destination.z - destination.x - 7.88746 < 0)
			{
				return vec3(0, 0, 0);
			}
			else
		    {
			   return normalize(vec3(-3.2671, 0, -3.2671)); //basso a sinistra
		    }
			
		}
		if (destination.z < -2.31018)
		{
			if (destination.x + destination.z + 7.88746 > 0)
			{
				return vec3(0, 0, 0);
			}
			else
			{
				return normalize(vec3(3.2671, 0, -3.2671)); //basso a destra
			}
			
		}
	}
}

GameObjIntWall::~GameObjIntWall()
{
}
