#pragma once
#include "GameObjBB.h"
#include "Animation.h"
#include "SceneLoader.h"
#include <vector>

class GameObjBB_SkeletalAnimation :
	public GameObjBB
{
public:

	class BoneWeights {
	public:
		aiMatrix4x4 offsetMatrix;
		std::vector<aiVertexWeight> weights;

		unsigned int boneId;
	};

	class Mesh {
	public:
		vector<aiVector3D> Vertices;
		vector<aiVector3D> Normals;
	};

	vector<vector<BoneWeights>> Weights;
	vector<Mesh> backupMeshes;
	SceneLoader *Loader;
	unsigned int AnimationId;
	GLfloat Time; // In secondi
	GLboolean Loop;

	GameObjBB_SkeletalAnimation(aiNode *meshEnable, const aiScene *scene, map<string, GLuint> textureIDMap, SceneLoader *loader, unsigned int animationId, GLboolean loop = false, vec3 rotationCenter = vec3(0, 0, 0), aiNode *meshDisable = NULL, ObjectState state = STATE_ENABLE, GLboolean render = GL_TRUE, GLfloat speed = (SPEED_OBJ), GLfloat rotationSpeed = ROTSPEED_OBJ);
	~GameObjBB_SkeletalAnimation();

	void updateFrame(double dt); //Fa calcolare il frame (specificato in Time dell'animazione identificata da AnimationId) dal SceneLoader e applica le trasformazioni del nuovo frame ai vertici e alle normali della mesh
	void drawFrame(); // Disegna la mesh all'ultimo frame calcolato

private:
	aiMatrix4x4 transformation;
	void calculateBoneTransformation(unsigned int boneId);
};

